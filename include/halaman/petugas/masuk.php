<?php


	try 
	{
		$sql		= "SELECT * FROM t_maintenance_web";
		$queryAdmin	= $koneksi->prepare($sql);
		$queryAdmin->execute();
		$dataAdmin = $queryAdmin->fetch();

		if($dataAdmin['status_web'] == "tidak_aktif"){
		header("location:".BASE_URL."error/403");
		}
	} 
	catch(Exception $e)
	{
		echo "ERROR QUERY ADD ADMIN KARENA <b>--".$e->getMessage()."--</b>";
			//header("location:".BASE_URL."error/query");
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Dashboard">
		<meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

		<title>MONITORING  - PANEL ADMIN</title>

		<!-- Bootstrap core CSS -->
		<link href="include/konfigurasi/assets/css/bootstrap.css" rel="stylesheet">
		<!--external css-->
		<link href="include/konfigurasi/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
		<!-- Custom styles for this template -->
		<link href="include/konfigurasi/assets/css/style.css" rel="stylesheet">
		<link href="include/konfigurasi/assets/css/style-responsive.css" rel="stylesheet">

		<!-- tooltip Login-->
		<link href="include/konfigurasi/css/login/login.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
	<!-- **********************************************************************************************************************************************************
		MAIN CONTENT
	*********************************************************************************************************************************************************** -->
	<div id="login-page">
		<div class="container">
			<form class="form-login" id="formLogin" action="query/login-admin" method="POST">
				<h2 class="form-login-heading">masuk sekarang</h2>
				<div class="login-wrap">	
					<span>Nama Pengguna</span>
					<input name="username" id="username" type="text" class="form-control" autofocus onChange="change_username()">
					<div id="tooltip_username"><span id="pesan_username"></span></div>
					<br>
					<span>Kata Sandi</span>
					<input name="password" id="password" type="password" class="form-control"onChange="change_password()">
					<div id="tooltip_password"><span id="pesan_password"></span></div>
					<br>
					<span id="tooltip_pesan"><span id="pesan_login">asdasd</span></span>
					<input style="background:#04863a;" class="btn btn-theme btn-block" type="submit" value="MASUK" onSubmit="login()"></button>		
					<div id="tooltip_pesan"><span id="pesan_login"></span></div>
				</div>
			</form>
			<form class="form-login" id="formEmail" action="" method="POST">
				<!-- Modal lupa sandi -->
				<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<input type="radio">HP
							<input type="radio">Email 
							<div style="background:#04863a;" class="modal-header">
								<h4 class="modal-title">Lupa Kata Sandi ?</h4>
							</div>
							<div class="modal-body">
								<p>Masukkan e-mail anda untuk mengatur ulang kata sandi</p>
								<input type="email" name="email" autocomplete="off" class="form-control placeholder-no-fix">
							</div>
							<div class="modal-footer">
								<button data-dismiss="modal" class="btn btn-default" type="button">Batal</button>
								<button style="background:#04863a;" class="btn btn-theme" type="button">Kirim</button>
							</div>
						</div>
					</div>
				</div>
				<!--  end modal lupa sandi-->
			</form>	  	
		</div>
	</div>

	<script src="include/konfigurasi/assets/js/jquery.js"></script>
	<script src="include/konfigurasi/assets/js/bootstrap.min.js"></script>

	<!-- AJAX LOGIN-->
	<script src="include/konfigurasi/js/login.js"></script>

	<!--BACKGROUND IMAGE-->
	<script type="text/javascript" src="include/konfigurasi/assets/js/jquery.backstretch.min.js"></script>
	<script>
		$.backstretch("include/konfigurasi/assets/img/login-bg.jpg", {speed: 1000});
	</script>
	</body>
</html>