
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin-super/beranda">PT. NUTRIFOOD</a>
            </div>

            <div class="header-right">

 
                <a href="query/logout-admin" class="btn btn-danger" title="Logout"><i class="fa fa-exclamation-circle fa-2x"></i></a>

            </div>
        </nav>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            

                            <div class="inner-text">
                                <?php echo $_SESSION['username']?>
                            <br />
                                <small>
                                    <?php
                                        try
                                        {
                                                
                                                $sql        = "SELECT admin_log_waktu 
                                                                FROM t_admin_log 
                                                                WHERE admin_log_admin='$_SESSION[id]' AND admin_log_keterangan='login'
                                                                ORDER BY admin_log_waktu DESC";
                                            $queryAdmin = $koneksi->prepare($sql);
                                            $queryAdmin->execute();
                                            $dataAdmin = $queryAdmin->fetch();
                                            
                                            $terakhir_masuk = date("D, d-M-Y H:i:s", strtotime($dataAdmin['admin_log_waktu']));  
                                            echo "<div style='font-size:11px; float:left'>Terakhir Masuk : ".terakhirMasuk($terakhir_masuk)."</div>";

                                        }
                                        catch(Exception $e)
                                        {
                                            $_SESSION['errorPesanQuery'] = "ERROR QUERY ADMIN KARENA <b>--".$e->getMessage()."--</b>";
                                            header("location:".BASE_URL."error/query");
                                        }
                                        
                                    ?>
                                </small>
                            </div>
                        </div>

                    </li>
