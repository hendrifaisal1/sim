<?php
	include "../../../../../../include/konfigurasi/query/koneksi.php";
	include "../../../../../../include/konfigurasi/query/fungsi.php";
	require_once "../../../../../../include/konfigurasi/PHPExcel.php";

	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);

	if (PHP_SAPI == 'cli')
		die('This example should only be run from a Web Browser');

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator($_SESSION['username'])
								 ->setTitle("Office 2007 XLSX Document")
								 ->setSubject("Nutrifood Document")
								 ->setDescription("Catatan Proses Mixing")
								 ->setCategory("Laporan");

	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.75);
	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');


	// Create the worksheet
	$objPHPExcel->setActiveSheetIndex(0);

	$style1 = new PHPExcel_Style();
	 
	$style1->applyFromArray(
	 array('borders' => array(
	 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
	 'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
	 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
	 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
	 ),
	 ));

	$f_rows=1;

	$objPHPExcel->getActiveSheet()->getStyle('A'.$f_rows.':I'.$f_rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$f_rows.':I'.$f_rows)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$f_rows, "Nama Mesin")
	 ->setCellValue('B'.$f_rows, "No Wo")
	 ->setCellValue('C'.$f_rows, "No Item")
	 ->setCellValue('D'.$f_rows, "No Batch")
	 ->setCellValue('E'.$f_rows, "Nama Item")
	 ->setCellValue('F'.$f_rows, "Jadwal Produksi")
	 ->setCellValue('G'.$f_rows, "Waktu Proses")
	 ->setCellValue('H'.$f_rows, "Waktu Selesai")
	 ->setCellValue('I'.$f_rows, "Lama Proses");
	$objPHPExcel->getActiveSheet()->getStyle('A'.$f_rows.':I'.$f_rows)->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->setTitle('Catatan Proses Mixing');
	$no=0;
	$rows = $f_rows;

	$sql		= "SELECT * FROM t_report_mixing 
					INNER JOIN t_mesin ON rm_mesin=mesin_id
					INNER JOIN t_wo ON rm_no_wo=wo_no
					INNER JOIN t_item ON wo_item=item_no
                    ORDER BY rm_id ASC";
	$queryAdmin	= $koneksi->prepare($sql);
	$queryAdmin->execute();

	foreach ($queryAdmin as $dataAdmin)
	{
		$rows++;
		$no++;

		if($dataAdmin['rm_waktu_jadwal'] != NULL)
			{$jadwal = date('d-M-Y' , strtotime($dataAdmin['rm_waktu_jadwal']));}
		else{$jadwal = "-";}
		if($dataAdmin['rm_waktu_proses'] != NULL)
			{$waktu_proses = date('d-M-Y H:i:s' , strtotime($dataAdmin['rm_waktu_proses']));}
		else{$waktu_proses = "-";}
		if($dataAdmin['rm_waktu_sudah'] != NULL)
			{$waktu_selesai = date('d-M-Y H:i:s' , strtotime($dataAdmin['rm_waktu_sudah']));}
		else{$waktu_selesai = "-";}

		if($dataAdmin['rm_status'] == "sudah")
		{
			$proses = (strtotime($dataAdmin['rm_waktu_sudah']) - strtotime($dataAdmin['rm_waktu_proses'])) / 60;
		}
		else if($dataAdmin['rm_status'] == "proses" || $dataAdmin['rm_status'] == "belum")
		{
			$proses = 0;
		}

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$rows, $dataAdmin['mesin_nama'])
		 ->setCellValue('B'.$rows, $dataAdmin['wo_no'])
		 ->setCellValue('C'.$rows, strtoupper($dataAdmin['item_no']))
		 ->setCellValue('D'.$rows, $dataAdmin['wo_no_batch'])
		 ->setCellValue('E'.$rows, $dataAdmin['item_nama'])
		 ->setCellValue('F'.$rows, $jadwal)
		 ->setCellValue('G'.$rows, $waktu_proses)
		 ->setCellValue('H'.$rows, $waktu_selesai)
		 ->setCellValue('I'.$rows, round($proses,2));
	}

	/*$ress = $db->query("SELECT * FROM t_casis where casis_jalur='1'");
	while ($r = $ress->fetch_assoc()) {
		$rows++;
		$no++;

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$rows, $r['casis_nama'])
		 ->setCellValue('B'.$rows, strtoupper($r['casis_sasal']))
		 ->setCellValue('C'.$rows, $r['casis_npun']);
	}*/

		$objPHPExcel->getActiveSheet()->getStyle('A'.($f_rows+1).':I'.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$f_rows.':I'.$rows)->getFont()->setSize(12);

	//Style
	/** Borders for all data */
	$objPHPExcel->getActiveSheet()->getStyle(
	    "A$f_rows:I$rows"
	)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(33);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);

	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Catatan Proses Mixing.xlsx"');
	header('Cache-Control: max-age=0');


	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
?>