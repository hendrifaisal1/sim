<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MONITORING PRD - ADMIN Panel</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="include/konfigurasi/assets2/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="include/konfigurasi/assets2/css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="include/konfigurasi/assets2/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="include/konfigurasi/assets2/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
       <?php include root."".rootPage."petugas/admin_biasa/head.php";?>


                    <li>
                        <a href="admin-biasa/beranda"><i class="fa fa-dashboard "></i>Beranda</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-desktop "></i>Manage Report Sacktipping <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-biasa/lihat-report-sacktipping"><i class="fa fa-bell "></i>Lihat Report Sacktipping</a>
                            </li>                      
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-yelp "></i>Manage Report Mixing  <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-biasa/lihat-report-mixing"><i class="fa fa-toggle-on"></i>Lihat Report Mixing</a>
                            </li>
                            
                        </ul>
                    </li>
                    <li>
                        <a class="active-menu-top" href="#"><i class="fa fa-flash "></i>Manage Report Discharge  <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level collapse in">
                             <li>
                                <a class="active-menu" href="admin-biasa/lihat-report-discharge"><i class="fa fa-toggle-on"></i>Lihat Report Discharge</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-bicycle "></i>Manage Report Filling <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-biasa/lihat-report-filling"><i class="fa fa-toggle-on"></i>Lihat Report Filling</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bicycle "></i>Manage Report Packing <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-biasa/lihat-report-packing"><i class="fa fa-toggle-on"></i>Lihat Report Packing</a>
                            </li>
                        </ul>
                    </li>                                      
                    <li>
                        <a href="admin-super/lock"><i class="fa fa-square-o "></i>Kunci Website</a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">DASHBOARD</h1>
                        <h1 class="page-subhead-line">This is dummy text , you can replace it with your original text. </h1>
                        <a href="include/halaman/petugas/admin_biasa/produksi/report_discharge/unduh_discharge.php">EXPORT DATA</a>
                    </div>
                </div>
                <!-- /. ROW  -->
               <div class="row">
                <div class="col-md-12">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Admin
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                <?php
								try 
								{
									setTimeZone();
									$sql		= "SELECT * FROM t_report_discharge  
													LEFT JOIN t_mesin ON rd_mesin=mesin_id
													INNER JOIN t_wo ON rd_no_wo=wo_no
                                                    LEFT JOIN t_report_qc ON rqc_no_wo=wo_no AND rqc_bagian=3
													INNER JOIN t_item ON wo_item=item_no
                                                    ORDER BY rd_id ASC";
									$queryAdmin	= $koneksi->prepare($sql);
									$queryAdmin->execute();
									$rowAdmin 	= $queryAdmin->rowCount();
									echo "
										<thead>
                                        <tr>
                                            <td>No WO</td>
                                            <td>Status WO</td>
                                            <td>Status QC</td>
                                            <td>Bin</td>
											<td>No Item</td>
											<td>No Batch</td>
											<td>Nama Item</td>
											<td>Status</td>
											<td>Jadwal</td>
                                            <td>Waktu Tampung</td>
                                            <td>Waktu Proses</td>
                                            <td>Waktu Selesai</td>
											<td>Lama Tampung</td>
											<td>Lama Tuang</td>
											<td>Lama Proses</td>
                                            <td>Aksi</td>
                                        </tr>
                                   		</thead>
                                   		<tbody>";
											$no = 1;
											foreach ($queryAdmin as $dtA)
											{
												if($dtA['rd_waktu_jadwal'] != NULL)
													{$jadwal = date('d-M-Y' , strtotime($dtA['rd_waktu_jadwal']));}
												else{$jadwal = "-";}
                                                if($dtA['rd_waktu_tampung'] != NULL)
                                                    {$waktu_tampung = date('d-M-Y H:i:s' , strtotime($dtA['rd_waktu_tampung']));}
                                                else{$waktu_tampung = "-";}
												if($dtA['rd_waktu_proses'] != NULL)
													{$waktu_proses = date('d-M-Y H:i:s' , strtotime($dtA['rd_waktu_proses']));}
												else{$waktu_proses = "-";}
												if($dtA['rd_waktu_sudah'] != NULL)
													{$waktu_selesai = date('d-M-Y H:i:s' , strtotime($dtA['rd_waktu_sudah']));}
												else{$waktu_selesai = "-";}
												

												if($dtA['rd_status'] == "belum")
													{
                                                        echo"<tr style='background:#FFB9B9'>";
                                                        $submit = "tampung";
                                                    }
                                                elseif ($dtA['rd_status'] == "tampung")
                                                    {
                                                        echo"<tr style='background:#ccc'>";
                                                        $submit = "proses";
                                                    }
												elseif ($dtA['rd_status'] == "proses_di_tuang")
													{
                                                        echo"<tr style='background:#F3B16C'>";
                                                        $submit = "selesai";
                                                    }
												elseif ($dtA['rd_status'] == "sudah_di_tuang")
													{echo"<tr style='background:#7EEC82'>";}

                                                 if($dtA["rqc_status"] == NULL){$statusQC = "-";}
                                                else{$statusQC = $dtA["rqc_status"];}
												echo"
                                                <form action='include/konfigurasi/query/back_end/admin-biasa/aksi.php?option=status_discharge' method='POST'>
														<td>$dtA[wo_no]</td>
                                                        <td>$dtA[wo_status]</td>
                                                        <td>$statusQC</td> 
                                                        <td>";
                                                        if($dtA['rd_status'] == "belum")
                                                        {
                                                            $mesin_id = $dtA['mesin_id'];
                                                            echo"
                                                                <select name='mesin'>
                                                                    <option value=0>PILIH BIN</option>
                                                                    ";
                                                                        $sq        = "SELECT mesin_id,mesin_nama FROM t_mesin WHERE mesin_bagian=3";
                                                                        $query = $koneksi->prepare($sq);
                                                                        $query->execute();
                                                                        foreach ($query as $dataAdmin)
                                                                        {
                                                                            echo"
                                                                                <option value='$dataAdmin[mesin_id]'>$dataAdmin[mesin_nama]</option>
                                                                            ";
                                                                        }      
                                                                echo"
                                                                </select>
                                                            ";
                                                        }
                                                        else
                                                        {
                                                            echo $dtA['mesin_nama'];
                                                        }
                                                        echo"</td>
														<td>$dtA[item_no]</td>
														<td>$dtA[wo_no_batch]</td>
														<td>$dtA[item_nama]</td>
														<td>$dtA[rd_status]</td>
														<td>$jadwal</td>
                                                        <td>$waktu_tampung</td>
                                                        <td>$waktu_proses</td>
                                                        <td>$waktu_selesai</td>";
														if($dtA['rd_status'] == "sudah_di_tuang")
                                                        {
                                                            $tampung = (strtotime($waktu_proses) - strtotime($waktu_tampung)) / 60;
                                                            $proses = (strtotime($waktu_selesai) - strtotime($waktu_proses)) / 60;
                                                            $sudah = $tampung + $proses;
                                                        }
                                                        else if($dtA['rd_status'] == "proses_di_tuang")
                                                        {
                                                            $tampung = (strtotime($waktu_proses) - strtotime($waktu_tampung)) / 60;
                                                            $proses = 0;
                                                            $sudah = $tampung + $proses;
                                                        }
                                                        else if($dtA['rd_status'] == "tampung")
                                                        {
                                                            $tampung = 0;
                                                            $proses = 0;
                                                            $sudah = $tampung + $proses;
                                                        }
                                                        else
                                                        {
                                                            $tampung = 0;
                                                            $proses = 0;
                                                            $sudah = $tampung + $proses;   
                                                        }

														
												echo"
                                                        <td>".round($tampung,2)." menit</td>
                                                        <td>".round($proses,2)." menit</td>
                                                        <td>".round($sudah,2)," menit</td>
													";
                                                        if($dtA['rd_status'] != "sudah_di_tuang")
                                                        {                                                  
                                                            echo"
                                                            
                                                                <td>
                                                                    <input type='hidden' value='$dtA[rd_id]' name='id'/>
                                                                    <input type='hidden' value='$submit' name='status'/>";
                                                                    if($dtA["wo_status"] == "pending"){
                                                                        echo"<button class='btn btn-inverse' disabled><i class='glyphicon glyphicon-edit'></i> $submit</button>";
                                                                    }
                                                                    else
                                                                    {
                                                                        if($dtA["rd_status"] == "belum"){
                                                                            echo"<button class='btn btn-inverse'><i class='glyphicon glyphicon-edit'></i> $submit</button>";
                                                                        }else{
                                                                            if($dtA["rqc_status"] == "tidak_ok" || $dtA["rqc_status"] == ""){echo"<button class='btn btn-inverse' disabled><i class='glyphicon glyphicon-edit'></i> $submit</button>";}
                                                                            else{echo"<button class='btn btn-inverse'><i class='glyphicon glyphicon-edit'></i> $submit</button>";}
                                                                        }
                                                                    }
                                                                    
                                                                    echo"
                                                                </td>
                                                            </form>";
                                                        }
                                                        else{echo"<td><button class='btn btn-danger' disabled>selesai</button>";}
                                                echo"
                                                    </tr>
                                                ";
												$no++;
											}
									echo"
											</tbody>
									";
								} 
								catch (Exception $e) 
								{
									echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
									//header("location:".BASE_URL."error/query");
								}
							?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
               
            </div>
                <!--/.ROW-->
                
                <!--/.ROW-->

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

   <?php include root."".rootPage."petugas/admin_biasa/foot.php";?>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/custom.js"></script>
    


</body>
</html>