<?php
	include "../../../../../../include/konfigurasi/query/koneksi.php";
	include "../../../../../../include/konfigurasi/query/fungsi.php";
	require_once "../../../../../../include/konfigurasi/PHPExcel.php";

	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);

	if (PHP_SAPI == 'cli')
		die('This example should only be run from a Web Browser');

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator($_SESSION['username'])
								 ->setTitle("Office 2007 XLSX Document")
								 ->setSubject("Nutrifood Document")
								 ->setDescription("Catatan Proses Sacktipping")
								 ->setCategory("Laporan");

	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.75);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.75);
	$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');


	// Create the worksheet
	$objPHPExcel->setActiveSheetIndex(0);

	$style1 = new PHPExcel_Style();
	 
	$style1->applyFromArray(
	 array('borders' => array(
	 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
	 'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
	 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
	 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
	 ),
	 ));

	$f_rows=1;

	$objPHPExcel->getActiveSheet()->getStyle('A'.$f_rows.':H'.$f_rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$f_rows.':H'.$f_rows)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$f_rows, "No Wo")
	 ->setCellValue('B'.$f_rows, "No Item")
	 ->setCellValue('C'.$f_rows, "No Batch")
	 ->setCellValue('D'.$f_rows, "Nama Item")
	 ->setCellValue('E'.$f_rows, "Jadwal Produksi")
	 ->setCellValue('F'.$f_rows, "Waktu Proses")
	 ->setCellValue('G'.$f_rows, "Waktu Selesai")
	 ->setCellValue('H'.$f_rows, "Lama Proses");
	$objPHPExcel->getActiveSheet()->getStyle('A'.$f_rows.':H'.$f_rows)->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->setTitle('Catatan Proses Sacktipping');
	$no=0;
	$rows = $f_rows;

	$sql		= "SELECT * FROM t_report_sacktipping 
					INNER JOIN t_wo ON rs_no_wo=wo_no
					INNER JOIN t_item ON wo_item=item_no
                    ORDER BY rs_id ASC";
	$queryAdmin	= $koneksi->prepare($sql);
	$queryAdmin->execute();

	foreach ($queryAdmin as $dataAdmin)
	{
		$rows++;
		$no++;

		if($dataAdmin['rs_waktu_jadwal'] != NULL)
			{$jadwal = date('d-M-Y' , strtotime($dataAdmin['rs_waktu_jadwal']));}
		else{$jadwal = "-";}
		if($dataAdmin['rs_waktu_proses'] != NULL)
			{$waktu_proses = date('d-M-Y H:i:s' , strtotime($dataAdmin['rs_waktu_proses']));}
		else{$waktu_proses = "-";}
		if($dataAdmin['rs_waktu_sudah'] != NULL)
			{$waktu_selesai = date('d-M-Y H:i:s' , strtotime($dataAdmin['rs_waktu_sudah']));}
		else{$waktu_selesai = "-";}

		if($dataAdmin['rs_status'] == "sudah")
		{
			$proses = (strtotime($dataAdmin['rs_waktu_sudah']) - strtotime($dataAdmin['rs_waktu_proses'])) / 60;
		}
		else if($dataAdmin['rs_status'] == "proses" || $dataAdmin['rs_status'] == "belum")
		{
			$proses = 0;
		}

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$rows, $dataAdmin['wo_no'])
		 ->setCellValue('B'.$rows, strtoupper($dataAdmin['item_no']))
		 ->setCellValue('C'.$rows, $dataAdmin['wo_no_batch'])
		 ->setCellValue('D'.$rows, $dataAdmin['item_nama'])
		 ->setCellValue('E'.$rows, $jadwal)
		 ->setCellValue('F'.$rows, $waktu_proses)
		 ->setCellValue('G'.$rows, $waktu_selesai)
		 ->setCellValue('H'.$rows, round($proses,2));
	}

	/*$ress = $db->query("SELECT * FROM t_casis where casis_jalur='1'");
	while ($r = $ress->fetch_assoc()) {
		$rows++;
		$no++;

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$rows, $r['casis_nama'])
		 ->setCellValue('B'.$rows, strtoupper($r['casis_sasal']))
		 ->setCellValue('C'.$rows, $r['casis_npun']);
	}*/

		$objPHPExcel->getActiveSheet()->getStyle('A'.($f_rows+1).':H'.$rows)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$f_rows.':H'.$rows)->getFont()->setSize(12);

	//Style
	/** Borders for all data */
	$objPHPExcel->getActiveSheet()->getStyle(
	    "A$f_rows:H$rows"
	)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(33);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);

	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Catatan Proses Sacktipping.xlsx"');
	header('Cache-Control: max-age=0');


	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
?>