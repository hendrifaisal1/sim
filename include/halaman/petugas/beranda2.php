

<head>
	<script type="text/javascript" src="include/konfigurasi/js/jquery.js"></script>
	<script type="text/javascript" src="include/konfigurasi/js/admin/admin.js"></script>

	<link rel="stylesheet" type="text/css" href="include/konfigurasi/css/main.css">
	<link rel="stylesheet" type="text/css" href="include/konfigurasi/css/admin/dasbor-admin.css">
</head>
<div id="wrapper">
	<div id="modal_edit_foto">
		<button class="close fr">X</button>
		<div class="cb"></div>
		<span class="judul_modal">GANTI FOTO PROFIL</span>
		<hr></hr>
		<div id="wrapper_form_edit_foto">
			<div class="form fl">
				<img class="foto_lama" src="include/images/web/user_man.png"/>
				<div class="cb mt_10"></div>
				<label>UNGGAH FOTO : </label>
				<div class="button_unggah">
					<span class="info_unggah">UNGGAH</span>
					<input id="file_unggah" type="file" onchange="unggah_foto(this);"/>
				</div>

				<div class="cb"></div>

				<div class="button_unggah_change" style="display:none">
					<span class="info_unggah_change">GANTI</span>
					<input  id="file_unggah_change" type="file" onchange="unggah_foto(this);"/>
				</div>
			</div>
			<div class="form fr">
				<img class="foto_baru"/>
				<div class="cb mt_5"></div>
				<label class="file_foto_baru fr" for="foto_baru"></label>
			</div>
			
		</div>
	</div>
	<div class="overlay_edit_foto"></div>
	<div id="wrapper_menu">
		<div id="wrapper_top_menu">
			<div id="wrapper_profil">
				<div id="wrapper_foto">
					<button class="edit_foto"></button>
					<div id="tooltip_edit">Ganti Foto Profil</div>
					<img src="include/images/web/user_man.png"/>
					<div class="info_foto"><?php echo "{$_SESSION['username']}";?></div>
				</div>
				<div class="cb mt_10"></div>
				<div id="wrapper_info">
					<div class="pesan"></div>
					<div class="cb"></div>
					<div class="atur_akun"></div>
				</div>
			</div>
		</div>
		<div id="wrapper_bottom_menu">
			<ul>
				<li>Menu 1
					<ul>
						<li>Sub Menu 1.1</li>
						<li>Sub Menu 1.2</li>
					</ul>
				</li>
				<li>Menu 2</li>
				<li>Menu 3</li>
			</ul>
		</div>
	</div>
	<?php
		echo "{$_SESSION['username']}<br/>";
	
	if($_SESSION['hakAkses'] == 'admin_super')
	{
		echo"
		<a href='admin-super/manage-admin'>MANAGE ADMIN</a><br/>
		<div class='cb'></div>
		<a href='admin-super/manage-website'>MANAGE WEBSITE</a><br/>
		<div class='cb'></div>
		<a href='admin-super/manage-departemen'>MANAGE DEPARTEMEN</a><br/>
		<div class='cb'></div>
		<a href='admin-super/manage-bagian'>MANAGE BAGIAN</a><br/>
		<div class='cb'></div>
		<a href='admin-super/manage-mesin'>MANAGE MESIN</a><br/>
		<div class='cb'></div>
		";
	}
	else
	{
		echo"
		<a href='admin-biasa/lihat-report-sacktipping'>MANAGE REPORT SACKTIPPING</a><br/>
		<a href='admin-biasa/lihat-report-mixing'>MANAGE REPORT MIXING</a><br/>
		<a href='admin-biasa/lihat-report-discharge'>MANAGE REPORT DISCHARGE</a><br/>
		<a href='admin-biasa/lihat-report-filling'>MANAGE REPORT FILLING</a><br/>
		<a href='admin-biasa/lihat-report-packing'>MANAGE REPORT PACKING</a><br/>
		<div class='cb'></div>
		";
	}


		try 
		{
			$sql		= "SELECT * FROM t_report_sacktipping rs
							INNER JOIN t_wo w ON w.wo_no=rs.rs_no_wo
							INNER JOIN t_item i ON i.item_no=w.wo_item";
			$querySack	= $koneksi->prepare($sql);
			$querySack->execute();
			$rowSack 	= $querySack->rowCount();
			echo "
				<p>BAGIAN SACKTIPPING</p>
					<table border=1>
						<tr>
							<td>No</td>
							<td>No Wo</td>
							<td>Nama Item</td>
							<td>No Batch</td>
							<td>Status</td>
						</tr>";
					$no = 1;
					foreach ($querySack as $dtS)
					{
						echo"
							<tr>
								<td>$no</td>
								<td>$dtS[rs_no_wo]</td>
								<td>$dtS[item_nama]</td>
								<td>$dtS[wo_no_batch]</td>
								<td>$dtS[rs_status]</td>
							</tr>
						";
						$no++;
					}
			echo"
					</table>
			";
		} 
		catch (Exception $e) 
		{
			echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
			//header("location:".BASE_URL."error/query");
		}

		try 
		{
			$sql		= "SELECT * FROM t_report_mixing rm
							INNER JOIN t_mesin m ON rm.rm_mesin=m.mesin_id
							INNER JOIN t_wo w ON w.wo_no=rm.rm_no_wo
							INNER JOIN t_item i ON i.item_no=w.wo_item";
			$queryMix	= $koneksi->prepare($sql);
			$queryMix->execute();
			$rowMix 	= $queryMix->rowCount();
			echo "
				<p>BAGIAN MIXING</p>
					<table border=1>
						<tr>
							<td>No</td>
							<td>Nama Mesin</td>
							<td>No Wo</td>
							<td>Nama Item</td>
							<td>No Batch</td>
							<td>Status</td>
						</tr>";
					$no = 1;
					foreach ($queryMix as $dtM)
					{
						echo"
							<tr>
								<td>$no</td>
								<td>$dtM[mesin_nama]</td>
								<td>$dtM[rm_no_wo]</td>
								<td>$dtM[item_nama]</td>
								<td>$dtM[wo_no_batch]</td>
								<td>$dtM[rm_status]</td>
							</tr>
						";
						$no++;
					}
			echo"
					</table>
			";
		} 
		catch (Exception $e) 
		{
			echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
			//header("location:".BASE_URL."error/query");
		}

		try 
		{
			$sql		= "SELECT * FROM t_report_discharge rd
							INNER JOIN t_mesin m ON rd.rd_mesin=m.mesin_id
							INNER JOIN t_wo w ON w.wo_no=rd.rd_no_wo
							INNER JOIN t_item i ON i.item_no=w.wo_item";
			$queryDis	= $koneksi->prepare($sql);
			$queryDis->execute();
			$rowDis 	= $queryDis->rowCount();
			echo "
				<p>BAGIAN DISCHARGE</p>
					<table border=1>
						<tr>
							<td>No</td>
							<td>Nama Mesin</td>
							<td>No Wo</td>
							<td>Nama Item</td>
							<td>No Batch</td>
							<td>Status</td>
						</tr>";
					$no = 1;
					foreach ($queryDis as $dtD)
					{
						echo"
							<tr>
								<td>$no</td>
								<td>$dtD[mesin_nama]</td>
								<td>$dtD[rd_no_wo]</td>
								<td>$dtD[item_nama]</td>
								<td>$dtD[wo_no_batch]</td>
								<td>$dtD[rd_status]</td>
							</tr>
						";
						$no++;
					}
			echo"
					</table>
			";
		} 
		catch (Exception $e) 
		{
			echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
			//header("location:".BASE_URL."error/query");
		}

		try 
		{
			$sql		= "SELECT * FROM t_report_filling rf
							INNER JOIN t_mesin m ON rf.rf_mesin=m.mesin_id
							INNER JOIN t_wo w ON w.wo_no=rf.rf_no_wo
							INNER JOIN t_item i ON i.item_no=w.wo_item";
			$queryFill	= $koneksi->prepare($sql);
			$queryFill->execute();
			$rowFill 	= $queryFill->rowCount();
			echo "
				<p>BAGIAN FILLING</p>
					<table border=1>
						<tr>
							<td>No</td>
							<td>Nama Mesin</td>
							<td>No Wo</td>
							<td>Nama Item</td>
							<td>No Batch</td>
							<td>Status</td>
						</tr>";
					$no = 1;
					foreach ($queryFill as $dtF)
					{
						echo"
							<tr>
								<td>$no</td>
								<td>$dtF[mesin_nama]</td>
								<td>$dtF[rf_no_wo]</td>
								<td>$dtF[item_nama]</td>
								<td>$dtF[wo_no_batch]</td>
								<td>$dtF[rf_status]</td>
							</tr>
						";
						$no++;
					}
			echo"
					</table>
			";
		} 
		catch (Exception $e) 
		{
			echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
			//header("location:".BASE_URL."error/query");
		}

		try 
		{
			$sql		= "SELECT * FROM t_report_packing rp
							INNER JOIN t_mesin m ON rp.rp_mesin=m.mesin_id
							INNER JOIN t_wo w ON w.wo_no=rp.rp_no_wo
							INNER JOIN t_item i ON i.item_no=w.wo_item";
			$queryPack	= $koneksi->prepare($sql);
			$queryPack->execute();
			$rowFill 	= $queryPack->rowCount();
			echo "
				<p>BAGIAN PACKING</p>
					<table border=1>
						<tr>
							<td>No</td>
							<td>Nama Mesin</td>
							<td>No Wo</td>
							<td>Nama Item</td>
							<td>No Batch</td>
							<td>Status</td>
						</tr>";
					$no = 1;
					foreach ($queryPack as $dtP)
					{
						echo"
							<tr>
								<td>$no</td>
								<td>$dtP[mesin_nama]</td>
								<td>$dtP[rp_no_wo]</td>
								<td>$dtP[item_nama]</td>
								<td>$dtP[wo_no_batch]</td>
								<td>$dtP[rp_status]</td>
							</tr>
						";
						$no++;
					}
			echo"
					</table>
			";
		} 
		catch (Exception $e) 
		{
			echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
			//header("location:".BASE_URL."error/query");
		}

	?>
	<a href="query/logout-admin">LOGOUT</a>
</div>