<?php
    setTimeZone();
    $_SESSION['page'] = $_SERVER['REQUEST_URI'];
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MONITORING PRD - ADMIN Panel</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="include/konfigurasi/assets2/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="include/konfigurasi/assets2/css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="include/konfigurasi/assets2/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="include/konfigurasi/assets2/css/custom.css" rel="stylesheet" />
   
</head>
<body>
    <div id="wrapper">
    <?php include root."".rootPage."petugas/admin_super/head.php"; ?>
                    <li>
                        <a href="admin-super/beranda"><i class="fa fa-dashboard "></i>Beranda</a>
                    </li>                
                    <li>
                        <a href="#"><i class="fa fa-desktop "></i>Manage Penyelia <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-super/tambah-penyelia"><i class="fa fa-toggle-on"></i>Tambah Penyelia</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-penyelia"><i class="fa fa-bell "></i>Lihat Penyelia</a>
                            </li>                      
                        </ul>
                    </li>
                     <li>
                        <a class="active-menu-top" href="#"><i class="fa fa-yelp "></i>Manage Departemen <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level collapse in">
                            <li>
                                <a class="active-menu" href="admin-super/tambah-departemen"><i class="fa fa-toggle-on"></i>Tambah Departemen</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-departemen"><i class="fa fa-bell "></i>Lihat Departemen</a>
                            </li>  
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-flash "></i>Manage Bagian <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                             <li>
                                <a href="admin-super/tambah-bagian"><i class="fa fa-toggle-on"></i>Tambah Bagian</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-bagian"><i class="fa fa-bell "></i>Lihat Bagian</a>
                            </li>  
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-bicycle "></i>Manage Mesin <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-super/tambah-mesin"><i class="fa fa-toggle-on"></i>Tambah Mesin</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-mesin"><i class="fa fa-bell "></i>Lihat Mesin</a>
                            </li> 
                        </ul>
                    </li>
                      <li>
                        <a href="admin-super/manage-website"><i class="fa fa-anchor "></i>Manage Website</a>
                    </li>
                                      
                    <li>
                        <a href="admin-super/lock"><i class="fa fa-square-o "></i>Kunci Website</a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">DASHBOARD</h1>
                       

                    </div>
                </div>
                <!-- /. ROW  -->
               <div class="row">
           	 <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-info">
                        <div class="panel-heading">
                           FORM TAMBAH DEPARTEMEN
                        </div>
                        <div class="panel-body">
                            <form role="form" action="include/konfigurasi/query/back_end/admin/aksi.php?option=tambah_departemen" method="POST">
                                        <div class="form-group">
                                            <label>Nama Departemen</label>
                                            <input class="form-control" type="text" name="nama">
                                            
                                        </div>
                                 <div class="form-group">
                                            <label>Singkatan</label>
                                            <input class="form-control" type="text" name="singkatan">
                                     
                                        </div>
                                                                      
                                 
                                        <button type="submit" class="btn btn-info">Tambah</button>

                                    </form>
                            </div>
                        </div>
                            </div>
				</div>
                <!--/.ROW-->

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <?php include root."".rootPage."petugas/admin_super/foot.php"; ?>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/custom.js"></script>
    


</body>
</html>