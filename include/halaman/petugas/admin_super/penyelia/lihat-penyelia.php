<?php
    setTimeZone();
    $_SESSION['page'] = $_SERVER['REQUEST_URI'];
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MONITORING PRD - ADMIN Panel</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="include/konfigurasi/assets2/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="include/konfigurasi/assets2/css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="include/konfigurasi/assets2/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="include/konfigurasi/assets2/css/custom.css" rel="stylesheet" />
    
</head>
<body>
    <div id="wrapper">
         <?php include root."".rootPage."petugas/admin_super/head.php"; ?>
                    <li>
                        <a href="admin-super/beranda"><i class="fa fa-dashboard "></i>Beranda</a>
                    </li>
                     <li>
                        <a class="active-menu-top" href="#"><i class="fa fa-desktop "></i>Manage Penyelia <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level collapse in">
                            <li>
                                <a href="admin-super/tambah-penyelia"><i class="fa fa-toggle-on"></i>Tambah Penyelia</a>
                            </li>
                            <li>
                                <a class="active-menu" href="admin-super/lihat-penyelia"><i class="fa fa-bell "></i>Lihat Penyelia</a>
                            </li>                      
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-yelp "></i>Manage Departemen <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-super/tambah-departemen"><i class="fa fa-toggle-on"></i>Tambah Departemen</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-departemen"><i class="fa fa-bell "></i>Lihat Departemen</a>
                            </li>  
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-flash "></i>Manage Bagian <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                             <li>
                                <a href="admin-super/tambah-bagian"><i class="fa fa-toggle-on"></i>Tambah Bagian</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-bagian"><i class="fa fa-bell "></i>Lihat Bagian</a>
                            </li>  
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-bicycle "></i>Manage Mesin <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-super/tambah-mesin"><i class="fa fa-toggle-on"></i>Tambah Mesin</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-mesin"><i class="fa fa-bell "></i>Lihat Mesin</a>
                            </li> 
                        </ul>
                    </li>
                      <li>
                        <a href="admin-super/manage-website"><i class="fa fa-anchor "></i>Manage Website</a>
                    </li>
                                      
                    <li>
                        <a href="admin-super/lock"><i class="fa fa-square-o "></i>Kunci Website</a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">DASHBOARD</h1>
                       

                    </div>
                </div>
                <!-- /. ROW  -->
               <div class="row">
                <div class="col-md-12">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Penyelia
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                <?php
								try 
								{
									$sql		= "SELECT * FROM t_admin a
													INNER JOIN t_admin_detail ad ON a.admin_id=ad.admin_detail_id
													INNER JOIN t_departemen d ON d.departemen_id=ad.admin_detail_departemen
													LEFT JOIN t_bagian b ON b.bagian_id=ad.admin_detail_bagian
                                                    WHERE admin_hak_akses='penyelia'";
									$queryAdmin	= $koneksi->prepare($sql);
									$queryAdmin->execute();
									$rowAdmin 	= $queryAdmin->rowCount();
									echo "
										<thead>
                                        <tr>
                                            <th>No</th>
                                            <td>Penyelia Nama</td>
											<td>Penyelia Departemen</td>
											<td>Penyelia Bagian</td>
                                            <td align='center' colspan='2'>Aksi</td>
                                        </tr>
                                   		</thead>
                                   		<tbody>";
											$no = 1;
											foreach ($queryAdmin as $dtA)
											{
												echo"
													<tr>
														<td>$no</td>
														<td>$dtA[admin_detail_nama]</td>
														<td>$dtA[departemen_nama]</td>
														<td>$dtA[bagian_nama]</td>
                                                        <td>
                                                            <form action='include/konfigurasi/query/back_end/admin/aksi.php?option=hapus_penyelia' method='POST'>
                                                                <input type='hidden' value='$dtA[admin_id]' name='id_admin'/>
                                                                <input class='btn btn-danger' type='submit' value='HAPUS'/>
                                                            </form>
                                                        </td>
													</tr>
												";
												$no++;
											}
									echo"
											</tbody>
									";
								} 
								catch (Exception $e) 
								{
									echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
									//header("location:".BASE_URL."error/query");
								}
							?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
               
            </div>
                <!--/.ROW-->

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    
    <?php include root."".rootPage."petugas/admin_super/foot.php"; ?>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/custom.js"></script>
    


</body>
</html>