<?php
    if($_SESSION['hakAkses'] == "admin_super"){include "admin_super/beranda.php";}
    else if($_SESSION['hakAkses'] == "admin_biasa")
    {
    	$id = $_SESSION['id'];
    	$sql		= "SELECT departemen_singkatan,bagian_nama FROM t_admin_detail 
    					INNER JOIN t_departemen ON admin_detail_departemen=departemen_id
    					LEFT JOIN t_bagian ON admin_detail_bagian=bagian_id
    					WHERE admin_detail_id=:id";
		$queryAdmin	= $koneksi->prepare($sql);
		$queryAdmin->bindParam(':id',$id,PDO::PARAM_STR);
		$queryAdmin->execute();
		$dataAdmin = $queryAdmin->fetch();

		if($dataAdmin["departemen_singkatan"] == "PRD")
		{
			include "admin_biasa/produksi/beranda.php";
		}
		else if($dataAdmin["departemen_singkatan"] == "PLB")
		{
			include "admin_biasa/gudang_baku/beranda.php";
		}
		else if($dataAdmin["departemen_singkatan"] == "PLG")
		{
			include "admin_biasa/gudang_jadi/beranda.php";
		}
		else if($dataAdmin["departemen_singkatan"] == "PQB")
		{
			include "admin_biasa/qc/beranda.php";
		}

    	//include "admin_biasa/beranda.php";
    }
    else if($_SESSION['hakAkses'] == "penyelia")
    {
    	/*$id = $_SESSION['id'];
    	$sql		= "SELECT departemen_singkatan,bagian_nama FROM t_admin_detail 
    					INNER JOIN t_departemen ON admin_detail_departemen=departemen_id
    					LEFT JOIN t_bagian ON admin_detail_bagian=bagian_id
    					WHERE admin_detail_id=:id";
		$queryAdmin	= $koneksi->prepare($sql);
		$queryAdmin->bindParam(':id',$id,PDO::PARAM_STR);
		$queryAdmin->execute();
		$dataAdmin = $queryAdmin->fetch();

		if($dataAdmin["departemen_singkatan"] == "PRD")
		{
			include "penyelia/produksi/beranda.php";
		}
		else if($dataAdmin["departemen_singkatan"] == "PLB")
		{
			include "penyelia/gudang_baku/beranda.php";
		}
		else if($dataAdmin["departemen_singkatan"] == "PLG")
		{
			include "penyelia/gudang_jadi/beranda.php";
		}
		else if($dataAdmin["departemen_singkatan"] == "PQB")
		{
			include "penyelia/qc/beranda.php";
		}*/

    	include "penyelia/produksi/beranda.php";
    }
?>