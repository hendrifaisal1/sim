<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MONITORING PRD - ADMIN Panel</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="include/konfigurasi/assets2/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="include/konfigurasi/assets2/css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="include/konfigurasi/assets2/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="include/konfigurasi/assets2/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
       <?php include root."".rootPage."petugas/penyelia/head.php"; ?>


                    <li>
                        <a class="active-menu" href="penyelia/beranda"><i class="fa fa-dashboard "></i>Beranda</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-desktop "></i>Manage Admin <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/tambah-admin"><i class="fa fa-toggle-on"></i>Tambah Admin</a>
                            </li>
                            <li>
                                <a href="penyelia/lihat-admin"><i class="fa fa-bell "></i>Lihat Admin</a>
                            </li>                      
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-desktop "></i>Manage Report Sacktipping <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-sacktipping"><i class="fa fa-bell "></i>Lihat Report Sacktipping</a>
                            </li>                      
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-yelp "></i>Manage Report Mixing  <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-mixing"><i class="fa fa-toggle-on"></i>Lihat Report Mixing</a>
                            </li>
                            
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-flash "></i>Manage Report Discharge  <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                             <li>
                                <a href="penyelia/lihat-reports-discharge"><i class="fa fa-toggle-on"></i>Lihat Report Discharge</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-bicycle "></i>Manage Report Filling <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-filling"><i class="fa fa-toggle-on"></i>Lihat Report Filling</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bicycle "></i>Manage Report Packing <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-packing"><i class="fa fa-toggle-on"></i>Lihat Report Packing</a>
                            </li>
                        </ul>
                    </li>                                      
                    <li>
                        <a href="admin-super/lock"><i class="fa fa-square-o "></i>Kunci Website</a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">DASHBOARD</h1>
                        

                    </div>
                </div>
                 <!-- /. ROW  -->
               <div class="row">
             <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-info">
                        <div class="panel-heading">
                           FORM IMPORT UBF
                        </div>
                        <div class="panel-body">
                            <form role="form" action="include/konfigurasi/query/aksi.php?option=import-ubf" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>File UBF</label>
                                            <input class="form-control" type="file" name="upload">
                                            
                                        </div>
                                 
                                  
                                 
                                        <button type="submit" class="btn btn-info">Tambah</button>

                                    </form>
                            </div>
                        </div>
                            </div>
                </div>
                <!--/.ROW-->

                 <!-- /. ROW  -->
               <div class="row">
             <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-info">
                        <div class="panel-heading">
                           FORM IMPORT UBP
                        </div>
                        <div class="panel-body">
                            <form role="form" action="include/konfigurasi/query/aksi.php?option=import-ubp" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>File UBP</label>
                                            <input class="form-control" type="file" name="upload">
                                            
                                        </div>
                                 
                                  
                                 
                                        <button type="submit" class="btn btn-info">Tambah</button>

                                    </form>
                            </div>
                        </div>
                            </div>
                </div>
                <!--/.ROW-->      

                 <!-- /. ROW  -->
               <div class="row">
             <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-info">
                        <div class="panel-heading">
                           FORM IMPORT MTOL
                        </div>
                        <div class="panel-body">
                            <form role="form" action="include/konfigurasi/query/aksi.php?option=import-mtol" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>File MTOL</label>
                                            <input class="form-control" type="file" name="upload">
                                            
                                        </div>
                                 
                                  
                                 
                                        <button type="submit" class="btn btn-info">Tambah</button>

                                    </form>
                            </div>
                        </div>
                            </div>
                </div>
                <!--/.ROW-->           

                
                

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

    <?php include root."".rootPage."petugas/penyelia/foot.php"; ?>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/custom.js"></script>
    


</body>
</html>