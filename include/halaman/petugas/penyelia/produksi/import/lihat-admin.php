<?php
    setTimeZone();
    $_SESSION['page'] = $_SERVER['REQUEST_URI'];
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MONITORING PRD - ADMIN Panel</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="include/konfigurasi/assets2/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="include/konfigurasi/assets2/css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="include/konfigurasi/assets2/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="include/konfigurasi/assets2/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin-super/beranda">PT. NUTRIFOOD</a>
            </div>

            <div class="header-right">

                <a href="message-task.html" class="btn btn-info" title="New Message"><b>30 </b><i class="fa fa-envelope-o fa-2x"></i></a>
                <a href="message-task.html" class="btn btn-primary" title="New Task"><b>40 </b><i class="fa fa-bars fa-2x"></i></a>
                <a href="query/logout-admin" class="btn btn-danger" title="Logout"><i class="fa fa-exclamation-circle fa-2x"></i></a>

            </div>
        </nav>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            <img src="include/konfigurasi/assets2/img/user.png" class="img-thumbnail" />

                            <div class="inner-text">
                                <?php echo $_SESSION['username']?>
                            <br />
                                <small>
                                    <?php
                                        try
                                        {
                                                
                                                $sql        = "SELECT admin_log_waktu 
                                                                FROM t_admin_log 
                                                                WHERE admin_log_admin='$_SESSION[id]' AND admin_log_keterangan='login'
                                                                ORDER BY admin_log_waktu DESC";
                                            $queryAdmin = $koneksi->prepare($sql);
                                            $queryAdmin->execute();
                                            $dataAdmin = $queryAdmin->fetch();
                                            
                                            $terakhir_masuk = date("D, d-M-Y H:i:s", strtotime($dataAdmin['admin_log_waktu']));  
                                            echo "<div style='float:left'>Terakhir Masuk : </p>".terakhirMasuk($terakhir_masuk);

                                        }
                                        catch(Exception $e)
                                        {
                                            $_SESSION['errorPesanQuery'] = "ERROR QUERY ADMIN KARENA <b>--".$e->getMessage()."--</b>";
                                            header("location:".BASE_URL."error/query");
                                        }
                                        
                                    ?>
                                </small>
                            </div>
                        </div>

                    </li>


                    <li>
                        <a href="admin-super/beranda"><i class="fa fa-dashboard "></i>Beranda</a>
                    </li>
                    <li>
                        <a class="active-menu-top" href="#"><i class="fa fa-desktop "></i>Manage Admin <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level collapse in">
                            <li>
                                <a href="admin-super/tambah-admin"><i class="fa fa-toggle-on"></i>Tambah Admin</a>
                            </li>
                            <li>
                                <a class="active-menu" href="admin-super/lihat-admin"><i class="fa fa-bell "></i>Lihat Admin</a>
                            </li>                      
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-desktop "></i>Manage Penyelia <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-super/tambah-penyelia"><i class="fa fa-toggle-on"></i>Tambah Penyelia</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-penyelia"><i class="fa fa-bell "></i>Lihat Penyelia</a>
                            </li>                      
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-yelp "></i>Manage Departemen <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-super/tambah-departemen"><i class="fa fa-toggle-on"></i>Tambah Departemen</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-departemen"><i class="fa fa-bell "></i>Lihat Departemen</a>
                            </li>  
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-flash "></i>Manage Bagian <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                             <li>
                                <a href="admin-super/tambah-bagian"><i class="fa fa-toggle-on"></i>Tambah Bagian</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-bagian"><i class="fa fa-bell "></i>Lihat Bagian</a>
                            </li>  
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-bicycle "></i>Manage Mesin <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="admin-super/tambah-mesin"><i class="fa fa-toggle-on"></i>Tambah Mesin</a>
                            </li>
                            <li>
                                <a href="admin-super/lihat-mesin"><i class="fa fa-bell "></i>Lihat Mesin</a>
                            </li> 
                        </ul>
                    </li>
                      <li>
                        <a href="admin-super/manage-website"><i class="fa fa-anchor "></i>Manage Website</a>
                    </li>
                                      
                    <li>
                        <a href="admin-super/lock"><i class="fa fa-square-o "></i>Kunci Website</a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">DASHBOARD</h1>
                        <h1 class="page-subhead-line">This is dummy text , you can replace it with your original text. </h1>

                    </div>
                </div>
                <!-- /. ROW  -->
               <div class="row">
                <div class="col-md-12">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Admin
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                <?php
								try 
								{
									$sql		= "SELECT * FROM t_admin a
													INNER JOIN t_admin_detail ad ON a.admin_id=ad.admin_detail_id
													INNER JOIN t_departemen d ON d.departemen_id=ad.admin_detail_departemen
													LEFT JOIN t_bagian b ON b.bagian_id=ad.admin_detail_bagian
                                                    WHERE admin_hak_akses='admin_biasa'";
									$queryAdmin	= $koneksi->prepare($sql);
									$queryAdmin->execute();
									$rowAdmin 	= $queryAdmin->rowCount();
									echo "
										<thead>
                                        <tr>
                                            <th>No</th>
                                            <td>Admin Nama</td>
											<td>Admin Departemen</td>
											<td>Admin Bagian</td>
                                        </tr>
                                   		</thead>
                                   		<tbody>";
											$no = 1;
											foreach ($queryAdmin as $dtA)
											{
												echo"
													<tr>
														<td>$no</td>
														<td>$dtA[admin_detail_nama]</td>
														<td>$dtA[departemen_nama]</td>
														<td>$dtA[bagian_nama]</td>
													</tr>
												";
												$no++;
											}
									echo"
											</tbody>
									";
								} 
								catch (Exception $e) 
								{
									echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
									//header("location:".BASE_URL."error/query");
								}
							?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
               
            </div>
                <!--/.ROW-->

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

    <div id="footer-sec">
        &copy; 2015 PT. Nutrifood | Design By : PT. Nutrifood
    </div>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/custom.js"></script>
    


</body>
</html>