<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MONITORING PRD - ADMIN Panel</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="include/konfigurasi/assets2/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="include/konfigurasi/assets2/css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="include/konfigurasi/assets2/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="include/konfigurasi/assets2/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <?php include root."".rootPage."petugas/penyelia/head.php"; ?>


                    <li>
                        <a href="admin-biasa/beranda"><i class="fa fa-dashboard "></i>Beranda</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-desktop "></i>Manage Admin <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/tambah-admin"><i class="fa fa-toggle-on"></i>Tambah Admin</a>
                            </li>
                            <li>
                                <a href="penyelia/lihat-admin"><i class="fa fa-bell "></i>Lihat Admin</a>
                            </li>                      
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-desktop "></i>Manage Report Sacktipping <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-sacktipping"><i class="fa fa-bell "></i>Lihat Report Sacktipping</a>
                            </li>                      
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-yelp "></i>Manage Report Mixing  <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-mixing"><i class="fa fa-toggle-on"></i>Lihat Report Mixing</a>
                            </li>
                            
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-flash "></i>Manage Report Discharge  <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                             <li>
                                <a href="penyelia/lihat-reports-discharge"><i class="fa fa-toggle-on"></i>Lihat Report Discharge</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a class="active-menu-top" href="#"><i class="fa fa-bicycle "></i>Manage Report Filling <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level collapse in">
                            <li>
                                <a class="active-menu" href="penyelia/lihat-reports-filling"><i class="fa fa-toggle-on"></i>Lihat Report Filling</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bicycle "></i>Manage Report Packing <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-packing"><i class="fa fa-toggle-on"></i>Lihat Report Packing</a>
                            </li>
                        </ul>
                    </li>                                      
                    <li>
                        <a href="admin-super/lock"><i class="fa fa-square-o "></i>Kunci Website</a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">DASHBOARD</h1>
                      

                    </div>
                </div>
                <!-- /. ROW  -->
               <div class="row">
                <div class="col-md-12">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Admin
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                <?php
								try 
								{
									setTimeZone();
									$sql		= "SELECT * FROM t_report_filling
													INNER JOIN t_mesin ON rf_mesin=mesin_id
													INNER JOIN t_wo ON rf_no_wo=wo_no
                                                    LEFT JOIN t_report_qc ON rqc_no_wo=wo_no AND rqc_bagian=4
													INNER JOIN t_item ON wo_item=item_no
                                                    ORDER BY rf_id ASC";
									$queryAdmin	= $koneksi->prepare($sql);
									$queryAdmin->execute();
									$rowAdmin 	= $queryAdmin->rowCount();
									echo "
										<thead>
                                        <tr>
                                            <td>No WO</td>
                                            <td>Status WO</td>
                                            <td>Status QC</td>
                                            <td>Mesin Filling</td>
											<td>No Item</td>
											<td>No Batch</td>
											<td>Nama Item</td>
											<td>Status</td>
											<td>Jadwal</td>
                                            <td>Waktu Proses</td>
                                            <td>Waktu Selesai</td>
											<td>Lama Proses</td>
                                        </tr>
                                   		</thead>
                                   		<tbody>";
											$no = 1;
											foreach ($queryAdmin as $dtA)
											{
												if($dtA['rf_waktu_jadwal'] != NULL)
													{$jadwal = date('d-M-Y' , strtotime($dtA['rf_waktu_jadwal']));}
												else{$jadwal = "-";}
												if($dtA['rf_waktu_proses'] != NULL)
													{$waktu_proses = date('d-M-Y H:i:s' , strtotime($dtA['rf_waktu_proses']));}
												else{$waktu_proses = "-";}
												if($dtA['rf_waktu_sudah'] != NULL)
													{$waktu_selesai = date('d-M-Y H:i:s' , strtotime($dtA['rf_waktu_sudah']));}
												else{$waktu_selesai = "-";}
												

												if($dtA['rf_status'] == "belum")
                                                    {
                                                        echo"<tr style='background:#FFB9B9'>";
                                                        $submit = "proses";
                                                    }
                                                elseif ($dtA['rf_status'] == "proses")
                                                    {
                                                        echo"<tr style='background:#F3B16C'>";
                                                        $submit = "selesai";
                                                    }
                                                elseif ($dtA['rf_status'] == "sudah")
                                                    {echo"<tr style='background:#7EEC82'>";}


                                                 if($dtA["rqc_status"] == NULL){$statusQC = "-";}
                                                else{$statusQC = $dtA["rqc_status"];}
												echo"
														<td>$dtA[wo_no]</td>
                                                         <td>$dtA[wo_status]</td>
                                                        <td>$statusQC</td> 
                                                        <td>$dtA[mesin_nama]</td>
														<td>$dtA[item_no]</td>
														<td>$dtA[wo_no_batch]</td>
														<td>$dtA[item_nama]</td>
														<td>$dtA[rf_status]</td>
														<td>$jadwal</td>
                                                        <td>$waktu_proses</td>
                                                        <td>$waktu_selesai</td>";
														if($dtA['rf_status'] == "sudah")
                                                        {
                                                            $proses = (strtotime($dtA['rf_waktu_sudah']) - strtotime($dtA['rf_waktu_proses'])) / 60;
                                                        }
                                                        else if($dtA['rf_status'] == "proses" || $dtA['rf_status'] == "belum")
                                                        {
                                                            $proses = 0;
                                                        }

														
												echo"
                                                        <td>".round($proses,2)." menit</td>
													";
                                                        
                                                echo"
                                                    </tr>
                                                ";
												$no++;
											}
									echo"
											</tbody>
									";
								} 
								catch (Exception $e) 
								{
									echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
									//header("location:".BASE_URL."error/query");
								}
							?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
               
            </div>
                <!--/.ROW-->
                
                <!--/.ROW-->

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

    <div id="footer-sec">
        &copy; 2015 PT. Nutrifood | Design By : PT. Nutrifood
    </div>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/custom.js"></script>
    


</body>
</html>