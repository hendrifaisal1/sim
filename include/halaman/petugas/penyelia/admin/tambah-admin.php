<?php
    $_SESSION['page'] = $_SERVER['REQUEST_URI'];
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MONITORING PRD - ADMIN Panel</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="include/konfigurasi/assets2/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="include/konfigurasi/assets2/css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="include/konfigurasi/assets2/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="include/konfigurasi/assets2/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
         <?php include root."".rootPage."petugas/penyelia/head.php"; ?>


                     <li>
                        <a href="penyelia/beranda"><i class="fa fa-dashboard "></i>Beranda</a>
                    </li>
                    <li>
                        <a class="active-menu-top" href="#"><i class="fa fa-desktop "></i>Manage Admin <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level collapse in">
                            <li>
                                <a class="active-menu"href="penyelia/tambah-admin"><i class="fa fa-toggle-on"></i>Tambah Admin</a>
                            </li>
                            <li>
                                <a href="penyelia/lihat-admin"><i class="fa fa-bell "></i>Lihat Admin</a>
                            </li>                      
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-desktop "></i>Manage Report Sacktipping <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-sacktipping"><i class="fa fa-bell "></i>Lihat Report Sacktipping</a>
                            </li>                      
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-yelp "></i>Manage Report Mixing  <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-mixing"><i class="fa fa-toggle-on"></i>Lihat Report Mixing</a>
                            </li>
                            
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-flash "></i>Manage Report Discharge  <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                             <li>
                                <a href="penyelia/lihat-reports-discharge"><i class="fa fa-toggle-on"></i>Lihat Report Discharge</a>
                            </li>
                        </ul>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-bicycle "></i>Manage Report Filling <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-filling"><i class="fa fa-toggle-on"></i>Lihat Report Filling</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bicycle "></i>Manage Report Packing <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="penyelia/lihat-reports-packing"><i class="fa fa-toggle-on"></i>Lihat Report Packing</a>
                            </li>
                        </ul>
                    </li>                                      
                    <li>
                        <a href="admin-super/lock"><i class="fa fa-square-o "></i>Kunci Website</a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">DASHBOARD</h1>
                       

                    </div>
                </div>
                <!-- /. ROW  -->
               <div class="row">
           	 <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-info">
                        <div class="panel-heading">
                           FORM TAMBAH ADMIN
                        </div>
                        <div class="panel-body">
                            <form role="form" action="include/konfigurasi/query/back_end/admin/aksi.php?option=tambah_admin" method="POST">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input class="form-control" type="text" name="username">
                                            
                                        </div>
                                 <div class="form-group">
                                            <label>Password</label>
                                            <input class="form-control" type="text" name="password">
                                     
                                        </div>
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input class="form-control" type="text" name="nama">
                                     
                                        </div>
                                         <div class="form-group">
                                            <label>Pilih Departemen</label>
                                            <select class="form-control" name="departemen">
											<option value="0">PILIH DEPARTEMEN</option>
											<?php
												try 
												{
													$sql		= "SELECT * FROM t_departemen";
													$querySack	= $koneksi->prepare($sql);
													$querySack->execute();
													$rowSack 	= $querySack->rowCount();
													echo "";
															foreach ($querySack as $dtS)
															{
																echo"
																	<option value=$dtS[departemen_id]>$dtS[departemen_singkatan]</option>
																";
																$no++;
															}
												} 
												catch (Exception $e) 
												{
													echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
													//header("location:".BASE_URL."error/query");
												}	
											?>
											</select>
                                        </div>
                                         
                                         <div class="form-group">
                                            <label>Pilih Bagian</label>
                                            <select class="form-control" name="bagian">
												<option value="0">PILIH BAGIAN</option>
											<?php
												try 
												{
													$sql		= "SELECT * FROM t_bagian";
													$querySack	= $koneksi->prepare($sql);
													$querySack->execute();
													$rowSack 	= $querySack->rowCount();
													echo "";
															foreach ($querySack as $dtS)
															{
																echo"
																	<option value=$dtS[bagian_id]>$dtS[bagian_nama]</option>
																";
																$no++;
															}
												} 
												catch (Exception $e) 
												{
													echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
													//header("location:".BASE_URL."error/query");
												}	
											?>
											</select>
                                        </div>
                                  
                                 
                                        <button type="submit" class="btn btn-info">Tambah</button>

                                    </form>
                            </div>
                        </div>
                            </div>
				</div>
                <!--/.ROW-->

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

    <div id="footer-sec">
        &copy; 2015 PT. Nutrifood | Design By : PT. Nutrifood
    </div>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/custom.js"></script>
    


</body>
</html>