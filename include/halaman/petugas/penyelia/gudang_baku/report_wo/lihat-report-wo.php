<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MONITORING PRD - ADMIN Panel</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="include/konfigurasi/assets2/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="include/konfigurasi/assets2/css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="include/konfigurasi/assets2/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="include/konfigurasi/assets2/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">PT. NUTRIFOOD</a>
            </div>

            <div class="header-right">

                <a href="message-task.html" class="btn btn-info" title="New Message"><b>30 </b><i class="fa fa-envelope-o fa-2x"></i></a>
                <a href="message-task.html" class="btn btn-primary" title="New Task"><b>40 </b><i class="fa fa-bars fa-2x"></i></a>
                <a href="query/logout-admin" class="btn btn-danger" title="Logout"><i class="fa fa-exclamation-circle fa-2x"></i></a>

            </div>
        </nav>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            <img src="include/konfigurasi/assets2/img/user.png" class="img-thumbnail" />

                            <div class="inner-text">
                                <?php echo $_SESSION['username']?>
                            <br />
                                <small>
                                    <?php
                                        try
                                        {
                                                
                                                $sql        = "SELECT admin_log_waktu 
                                                                FROM t_admin_log 
                                                                WHERE admin_log_admin='$_SESSION[id]' AND admin_log_keterangan='login'
                                                                ORDER BY admin_log_waktu DESC";
                                            $queryAdmin = $koneksi->prepare($sql);
                                            $queryAdmin->execute();
                                            $dataAdmin = $queryAdmin->fetch();
                                            
                                            $terakhir_masuk = date("D, d-M-Y H:i:s", strtotime($dataAdmin['admin_log_waktu']));  
                                            echo "<div style='float:left'>Terakhir Masuk : </p>".terakhirMasuk($terakhir_masuk);

                                        }
                                        catch(Exception $e)
                                        {
                                            $_SESSION['errorPesanQuery'] = "ERROR QUERY ADMIN KARENA <b>--".$e->getMessage()."--</b>";
                                            header("location:".BASE_URL."error/query");
                                        }
                                        
                                    ?>
                                </small>
                            </div>
                        </div>

                    </li>


                    <li>
                        <a href="admin-biasa/beranda"><i class="fa fa-dashboard "></i>Beranda</a>
                    </li>
                    <li>
                        <a class="active-menu-top" href="#"><i class="fa fa-desktop "></i>Manage WO <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level collapse in">
                            <li>
                                <a class="active-menu" href="admin-biasa/lihat-report-sacktipping"><i class="fa fa-bell "></i>Lihat Report WO</a>
                            </li>                      
                        </ul>
                    </li>                                    
                    <li>
                        <a href="admin-super/lock"><i class="fa fa-square-o "></i>Kunci Website</a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">DASHBOARD</h1>
                        <h1 class="page-subhead-line">This is dummy text , you can replace it with your original text. </h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
                <div class="col-md-12">
                     <!--    Hover Rows  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Admin
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                <?php
                                try 
                                {
                                    setTimeZone();
                                    $sql        = "SELECT * FROM t_wo
                                                    INNER JOIN t_item ON wo_item=item_no
                                                    ORDER BY wo_no_batch ASC";
                                    $queryAdmin = $koneksi->prepare($sql);
                                    $queryAdmin->execute();
                                    $rowAdmin   = $queryAdmin->rowCount();
                                    echo "
                                        <thead>
                                        <tr>
                                            <td>No WO</td>
                                            <td>No Item</td>
                                            <td>No Batch</td>
                                            <td>Nama Item</td>
                                            <td>Status</td>
                                            <td>Waktu Pending</td>
                                            <td>Waktu Wip</td>
                                            <td>Waktu Complete</td>
                                            <td>Waktu Closed</td>
                                            <td>Waktu Canceled</td>
                                        </tr>
                                        </thead>
                                        <tbody>";
                                            $no = 1;
                                            foreach ($queryAdmin as $dtA)
                                            {
                                                if($dtA['wo_waktu_pending'] != NULL)
                                                    {$pending = date('d-M-Y' , strtotime($dtA['wo_waktu_pending']));}
                                                else{$pending = "-";}
                                                if($dtA['wo_waktu_wip'] != NULL)
                                                    {$wip = date('d-M-Y H:i:s' , strtotime($dtA['wo_waktu_wip']));}
                                                else{$wip = "-";}
                                                if($dtA['wo_waktu_complete'] != NULL)
                                                    {$complete = date('d-M-Y H:i:s' , strtotime($dtA['wo_waktu_complete']));}
                                                else{$complete = "-";}
                                                if($dtA['wo_waktu_closed'] != NULL)
                                                    {$closed = date('d-M-Y H:i:s' , strtotime($dtA['wo_waktu_closed']));}
                                                else{$closed = "-";}
                                                 if($dtA['wo_waktu_canceled'] != NULL)
                                                    {$canceled = date('d-M-Y H:i:s' , strtotime($dtA['wo_waktu_canceled']));}
                                                else{$canceled = "-";}
                                                

                                                if($dtA['wo_status'] == "pending")
                                                    {
                                                        echo"<tr style='background:#FFB9B9'>";
                                                        $submit = "wip";
                                                    }
                                                elseif ($dtA['wo_status'] == "wip")
                                                    {
                                                        echo"<tr style='background:#F3B16C'>";
                                                        $submit = "complete";
                                                    }
                                                elseif ($dtA['wo_status'] == "complete")
                                                    {
                                                        echo"<tr style='background:#7EEC82'>";
                                                        $submit = "closed";
                                                    }
                                                 elseif ($dtA['wo_status'] == "closed")
                                                    {echo"<tr style='background:#cacaca'>";}
                                                elseif ($dtA['wo_status'] == "canceled")
                                                    {echo"<tr style='background:#FFB9B9'>";}
                                                echo"
                                                        <td>$dtA[wo_no]</td>
                                                        <td>$dtA[item_no]</td>
                                                        <td>$dtA[wo_no_batch]</td>
                                                        <td>$dtA[item_nama]</td>
                                                        <td>$dtA[wo_status]</td>
                                                        <td>$pending</td>
                                                        <td>$wip</td>
                                                        <td>$complete</td>
                                                         <td>$closed</td>
                                                          <td>$canceled</td>";
                                                        if($dtA['wo_status'] == "pending")
                                                        {                                                  
                                                            echo"
                                                            <form action='include/konfigurasi/query/back_end/admin-biasa/aksi.php?option=status_wo' method='POST'>
                                                                <td>
                                                                    <input type='hidden' value='$dtA[wo_no]' name='id'/>
                                                                    <input type='hidden' value='wip' name='status'/>
                                                                    <button class='btn btn-inverse'><i class='glyphicon glyphicon-edit'></i> proses</button>
                                                                </td>
                                                            </form>";
                                                        }
                                                        elseif ($dtA['wo_status'] == "closed") {
                                                            echo"<td><button class='btn btn-danger' disabled>selesai</button>";
                                                        }
                                                        else{echo"<td><button class='btn btn-danger' disabled>$submit</button>";}
                                                echo"
                                                    </tr>
                                                ";
                                                $no++;
                                            }
                                    echo"
                                            </tbody>
                                    ";
                                } 
                                catch (Exception $e) 
                                {
                                    echo "ERROR QUERY SACKTIPPING KARENA <b>--".$e->getMessage()."--</b>";
                                    //header("location:".BASE_URL."error/query");
                                }
                            ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End  Hover Rows  -->
                </div>
               
            </div>
                <!--/.ROW-->

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

    <div id="footer-sec">
        &copy; 2015 PT. Nutrifood | Design By : PT. Nutrifood
    </div>
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="include/konfigurasi/assets2/js/custom.js"></script>
    


</body>
</html>