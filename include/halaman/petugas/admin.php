<?php
	$admin = $_GET['admin'];

	if(isset($admin))
	{
		switch ($admin)
		{
			case 'masuk'						:	sessionNoLogin();	include root."".rootPage."/petugas/masuk.php";															break;
			case 'masuk-super'						:	sessionNoLogin();	include root."".rootPage."/petugas/masuk-super.php";															break;
			case 'beranda'						:	sessionLogin();		include root."".rootPage."/petugas/beranda.php";															break;
			case 'lock'							:	sessionLogin();		include root."".rootPage."/petugas/lock.php";																break;

			// SUPER ADMIN
			case 'manage-website'				:	sessionLogin();		include root."".rootPage."/petugas/admin_super/manage-website.php";										break;			
			case 'manage-penyelia'				:	sessionLogin();		include root."".rootPage."/petugas/admin_super/penyelia/manage-penyelia.php";								break;
			case 'lihat-penyelia'				:	sessionLogin();		include root."".rootPage."/petugas/admin_super/penyelia/lihat-penyelia.php";								break;
			case 'tambah-penyelia'				:	sessionLogin();		include root."".rootPage."/petugas/admin_super/penyelia/tambah-penyelia.php";								break;
			case 'manage-departemen'			:	sessionLogin();		include root."".rootPage."/petugas/admin_super/departemen/manage-departemen.php";							break;
			case 'lihat-departemen'				:	sessionLogin();		include root."".rootPage."/petugas/admin_super/departemen/lihat-departemen.php";							break;
			case 'tambah-departemen'			:	sessionLogin();		include root."".rootPage."/petugas/admin_super/departemen/tambah-departemen.php";							break;
			case 'edit-departemen'			:	sessionLogin();		include root."".rootPage."/petugas/admin_super/departemen/edit-departemen.php";							break;
			case 'manage-bagian'				:	sessionLogin();		include root."".rootPage."/petugas/admin_super/bagian/manage-bagian.php";									break;
			case 'lihat-bagian'					:	sessionLogin();		include root."".rootPage."/petugas/admin_super/bagian/lihat-bagian.php";									break;
			case 'tambah-bagian'				:	sessionLogin();		include root."".rootPage."/petugas/admin_super/bagian/tambah-bagian.php";									break;
			case 'edit-bagian'				:	sessionLogin();		include root."".rootPage."/petugas/admin_super/bagian/edit-bagian.php";									break;
			case 'manage-mesin'					:	sessionLogin();		include root."".rootPage."/petugas/admin_super/mesin/manage-mesin.php";									break;
			case 'lihat-mesin'					:	sessionLogin();		include root."".rootPage."/petugas/admin_super/mesin/lihat-mesin.php";									break;
			case 'tambah-mesin'					:	sessionLogin();		include root."".rootPage."/petugas/admin_super/mesin/tambah-mesin.php";									break;
			case 'edit-mesin'					:	sessionLogin();		include root."".rootPage."/petugas/admin_super/mesin/edit-mesin.php";								break;

			// PENYELIA
			case 'manage-admin'					:	sessionLogin();		include root."".rootPage."/petugas/penyelia/admin/manage-admin.php";											break;
			case 'lihat-admin'					:	sessionLogin();		include root."".rootPage."/petugas/penyelia/admin/lihat-admin.php";												break;
			case 'tambah-admin'					:	sessionLogin();		include root."".rootPage."/petugas/penyelia/admin/tambah-admin.php";											break;
			case 'import'						:	sessionLogin();		include root."".rootPage."/petugas/admin_super/import/tambah-admin.php";											break;
			case 'lihat-reports-sacktipping'		:	sessionLogin();		include root."".rootPage."/petugas/penyelia/produksi/report_sacktipping/lihat-report-sacktipping.php";	break;
			case 'lihat-reports-mixing'			:	sessionLogin();		include root."".rootPage."/petugas/penyelia/produksi/report_mixing/lihat-report-mixing.php";			break;
			case 'lihat-reports-discharge'		:	sessionLogin();		include root."".rootPage."/petugas/penyelia/produksi/report_discharge/lihat-report-discharge.php";		break;
			case 'lihat-reports-filling'			:	sessionLogin();		include root."".rootPage."/petugas/penyelia/produksi/report_filling/lihat-report-filling.php";			break;
			case 'lihat-reports-packing'			:	sessionLogin();		include root."".rootPage."/petugas/penyelia/produksi/report_packing/lihat-report-packing.php";			break;

			// ADMIN
			case 'lihat-report-sacktipping'		:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/produksi/report_sacktipping/lihat-report-sacktipping.php";	break;
			case 'lihat-report-mixing'			:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/produksi/report_mixing/lihat-report-mixing.php";			break;
			case 'lihat-report-discharge'		:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/produksi/report_discharge/lihat-report-discharge.php";		break;
			case 'lihat-report-filling'			:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/produksi/report_filling/lihat-report-filling.php";			break;
			case 'lihat-report-packing'			:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/produksi/report_packing/lihat-report-packing.php";			break;
			case 'lihat-report-wo'				:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/gudang_baku/report_wo/lihat-report-wo.php";				break;
			case 'lihat-report-qc-sacktipping'	:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/qc/report_sacktipping/lihat-report-qc-sacktipping.php";	break;
			case 'lihat-report-qc-discharge'	:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/qc/report_discharge/lihat-report-qc-discharge.php";		break;
			case 'lihat-report-qc-filling'		:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/qc/report_filling/lihat-report-qc-filling.php";			break;
			case 'lihat-report-qc-packing'		:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/qc/report_packing/lihat-report-qc-packing.php";			break;
			case 'lihat-report-wo2'				:	sessionLogin();		include root."".rootPage."/petugas/admin_biasa/gudang_jadi/report_wo/lihat-report-wo.php";				break;
			default 							:						include root."".rootPage."error/404.html";																break;
		}
	}
?>