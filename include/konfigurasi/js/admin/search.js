var keyword;

$(document).ready(function(){
	$("#search").change(function(){
		keyword = $("#search").val();

		if(keyword != ""){
			document.getElementById("button_hapus").style.visibility = "visible";
			$("#button_hapus").fadeTo(100,1);
			$("#tooltip_search").fadeTo(500,0);
		}else{
			$("#button_hapus").fadeTo(100,0);
		}
	});

	$("#button_hapus").click(function(){
		$("#search").val("");
		
		document.getElementById("button_hapus").style.visibility = "hidden";
		$("#button_hapus").fadeTo(100,0);
	});

	$("#button_search").click(function(){
		keyword = $("#search").val();
		
		if(keyword == ""){
			document.getElementById("tooltip_search").style.visibility = "visible";
			$("#tooltip_search").fadeTo(500,1);
			$("#tooltip_search").html("Kata Kunci Tidak Boleh Kosong");
		}else{
			document.getElementById("tooltip_search").style.visibility = "visible";
			$("#tooltip_search").html("Tunggu Sebentar...");
			$("#tooltip_search").fadeTo(500,1);
			setTimeout(
				function(){
				window.location.assign("admin/beranda")
			},3000);
		}
	});
});

/*COPYRIGHT KU STUDIOS*/