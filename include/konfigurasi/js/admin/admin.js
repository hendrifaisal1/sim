var value_username, cek_username, option;

function change_username()
{
	value_username 	= $("#username").val();
	cek_username 	= false;

	if(value_username == "")
	{
		document.getElementById("tooltip_username").style.visibility = "visible";
		$("#tooltip_username").fadeTo(500,1);
		$("#pesan_username").text("username Admin Tidak Boleh Kosong");
	}
	else
	{
		option = "cek_admin";

		$.ajax(
		{
			url			: "include/konfigurasi/query/back_end/aksi.php",
			data		: "option="+option+"&username="+username,
			cache		: false,
			beforeSend	: function() 
			{
				document.getElementById("tooltip_pesan").style.visibility = "visible";
				$("#tooltip_pesan").fadeTo(500,1);
				$("#pesan_login").text("Tunggu Sebentar ...");
			},
			success		: function(msg)
			{
				if(msg == "sukses")
				{
					setTimeout(
						function()
						{
							window.location.assign("admin/beranda")
						},3000
					);
				}
				else
				{
					setTimeout(
						function()
						{
							$("#pesan_login").text(msg);
						},3000
					);
				}
			}
		}
		);
		cek_user = true;
		$("#tooltip_username").fadeTo(500,0);
		$("#pesan_username").text("username Admin Tidak Boleh Kosong");
	}
}

function reset_pesan()
{
	$("#tooltip_pesan").fadeTo(500,0);
}

function tambah()
{
	change_username();
	option = "tambah-admin";

	if(cek_username == true)
	{
		$.ajax(
		{
			url			: "include/konfigurasi/query/aksi.php",
			data		: "option="+option+"&username="+username+"&password="+password,
			cache		: false,
			beforeSend	: function() 
			{
				document.getElementById("tooltip_pesan").style.visibility = "visible";
				$("#tooltip_pesan").fadeTo(500,1);
				$("#pesan_login").text("Tunggu Sebentar ...");
			},
			success		: function(msg)
			{
				if(msg == "sukses")
				{
					setTimeout(
						function()
						{
							window.location.assign("admin/beranda")
						},3000
					);
				}
				else
				{
					setTimeout(
						function()
						{
							$("#pesan_login").text(msg);
						},3000
					);
				}
			}
		}
		);
	}
}

$(document).ready(function()
{
	$("#username").change(function()
	{
		change_username();
		reset_pesan();
	});

	$("#username").click(function()
	{
		reset_pesan();
	});

	$("#submit_tambah").click(function()
	{
		reset_pesan();
		tambah();
	});
});

/*COPYRIGHT KU STUDIOS*/