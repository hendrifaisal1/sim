var value_button, value_button_change, option;

/*UNGGAH FOTO*/
function unggah_foto(input)
{
	option 				= "edit-foto";
	value_button 		= $("span.info_unggah").text();
	value_button_change = $("span.info_unggah_change").text();

	if(value_button == "UNGGAH" || value_button_change == "GANTI")
	{
		document.getElementById("file_unggah").style.visibility = "hidden";
		$("button.simpan").css({"display":"block"});
		$("span.info_unggah").text("SIMPAN");

		$("div.button_unggah_change").css({"display":"block"});

		if (input.files && input.files[0]) 
		{
			var reader = new FileReader(); 
			 
			reader.onload = function (e) 
			{ 
				
				$('img.foto_baru').attr('src', e.target.result);
			};
			 
			reader.readAsDataURL(input.files[0]);
		}
	}
}

function simpan_foto()
{
	document.getElementById("file_unggah").style.visibility = "visible";
	$("span.info_unggah").text("UNGGAH");
	$("div.button_unggah_change").css({"display":"none"});
}

function preview_gambar(input) 
{
	if (input.files && input.files[0]) 
	{
		var reader = new FileReader(); 
		 
		reader.onload = function (e) 
		{ 
			sesi = true;
			$('img.foto_baru').attr('src', e.target.result);
		};
		 
		reader.readAsDataURL(input.files[0]);
	}
}

/*POPUP MODAL*/
function show_modal()
{
	$("#modal_edit_foto").css({"transform":"scale3d(1,1,1)"});
	$("#modal_edit_foto").addClass("modal_show");
}

function hide_modal()
{
	$("#modal_edit_foto").removeClass("modal_show");
	$("#modal_edit_foto").css({"transform":"scale3d(0,0,0)"});
}

$(document).ready(function()
{
	$("div.button_unggah").click(function()
	{
		simpan_foto();
	});

	$("button.close").click(function()
	{
		hide_modal();
	});

	$("div.overlay").click(function()
	{
		hide_modal();
	});

	$("button.edit_foto").click(function()
	{
		show_modal();
	});
});

/*COPYRIGHT KU STUDIOS*/