var username, password, option, cek_user, cek_pass;

function change_username()
{
	username 	= $("#username").val();
	cek_user	= false;

	if(username == "")
	{
		document.getElementById("tooltip_username").style.visibility = "visible";
		$("#tooltip_username").fadeTo(500,1);
		$("#pesan_username").text("Nama Pengguna Tidak Boleh Kosong");
	}
	else
	{
		cek_user = true;
		$("#tooltip_username").fadeTo(500,0);
		$("#pesan_username").text("Nama Pengguna Tidak Boleh Kosong");
	}
}

function change_password()
{
	password 	= $("#password").val();
	cek_pass	= false;

	if(password == "")
	{
		document.getElementById("tooltip_password").style.visibility = "visible";
		$("#tooltip_password").fadeTo(500,1);
		$("#pesan_password").text("Kata Sandi Tidak Boleh Kosong");
	}
	else
	{
		cek_pass = true;
		$("#tooltip_password").fadeTo(500,0);
		$("#pesan_password").text("Kata Sandi Tidak Boleh Kosong");
	}
}

function reset_pesan()
{
	$("#tooltip_pesan").fadeTo(500,0);
}

function login()
{
	change_username();
	change_password();

	if(cek_user == true && cek_pass == true)
	{

		$().ajaxStart(function() {
			document.getElementById("tooltip_pesan").style.visibility = "visible";
			$("#tooltip_pesan").fadeTo(500,1);
			$("#pesan_login").text("Tunggu Sebentar ...");
		}).ajaxStop(function() {
			$("#tooltip_pesan").fadeTo(500,0);
			$("#pesan_login").text("Tunggu Sebentar ...");
		});

		$.ajax(
		{
			type		: "POST",
			url			: $("#formLogin").attr('action'),
			data		: $("#formLogin").serialize(),
			cache		: false,
			success		: function(msg)
			{
				if(msg == "admin_super")
				{
					setTimeout(
						function()
						{
							window.location.href = "admin-super/beranda";
						},3000
					);
				}
				else if(msg == "admin_biasa")
				{
					setTimeout(
						function()
						{
							window.location.assign("admin-biasa/beranda")
						},3000
					);
				}
				else if(msg == "penyelia")
				{
					setTimeout(
						function()
						{
							window.location.assign("penyelia/beranda")
						},3000
					);
				}
				else
				{
					setTimeout(
						function()
						{
							$("#pesan_login").text(msg);
						},3000
					);
				}
			}
		}
		);
		return false;
	}
}

$(document).ready(function(e)
{
	// e.preventDefault;
	/*$("#username").change(function()
	{
		change_username();
		reset_pesan();
	});

	$("#password").change(function()
	{
		change_password();
		reset_pesan();
	});

	$("#username").click(function()
	{
		reset_pesan();
	});

	$("#password").click(function(){
		reset_pesan();
	});

	$("#submit_login").click(function()
	{
		reset_pesan();
		login();
	});*/
});

/*COPYRIGHT KU STUDIOS*/