var username, cUsername, cPassword;

function cekUsername()
{
	username 	= $("#username").val();
	cUsername	= false;

	if(username == "")
	{
		document.getElementById("tooltip_username").style.visibility = "visible";
		$("#tooltip_username").fadeTo(500,1);
		$("#pesan_username").text("Nama Pengguna Tidak Boleh Kosong");
	}
	else
	{
		cUsername = true;
		$("#tooltip_username").fadeTo(500,0);
		$("#pesan_username").text("Nama Pengguna Tidak Boleh Kosong");
	}
}

function cekPassword()
{
	password 	= $("#password").val();
	cPassword	= false;

	if(password == "")
	{
		document.getElementById("tooltip_password").style.visibility = "visible";
		$("#tooltip_password").fadeTo(500,1);
		$("#pesan_password").text("Kata Sandi Tidak Boleh Kosong");
	}
	else
	{
		cPassword = true;
		$("#pesan_password").fadeTo(500,0);
		$("#pesan_password").text("Kata Sandi Tidak Boleh Kosong");
	}
}

function login()
{
	cekUsername();
	cekPassword();

	if(cUsername == true && cPassword == true)
	{
		$.ajax(
		{
			url			: "include/konfigurasi/query/aksi.php?option=login-admin",
			data		: $("#formLogin").serialize(),
			cache		: false,
			beforeSend	: function() 
			{
				document.getElementById("tooltip_pesan").style.visibility = "visible";
				$("#tooltip_pesan").fadeTo(500,1);
				$("#pesan_login").text("Tunggu Sebentar ...");
			},
			success		: function(msg)
			{
				//alert(msg);
				if(msg == "sukses")
				{
					// setTimeout(
					// 	function()
					// 	{
							window.location.assign("admin-super/beranda")
					// 	},3000
					// );
				}
				else
				{
					//alert('err');
					// setTimeout(
					// 	function()
					// 	{
							$("#pesan_login").text(msg);
					// 	},3000
					// );
				
				}
			}
		}
		);
	}
}

// $(document).ready(function(e)
// {
// 	// e.preventDefault;

// 	$("#submit_login").click(function()
// 	{
// 		login();
// 	});
// });