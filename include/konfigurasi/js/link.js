var option;

function option_manage()
{
	var location 	= window.location.href;
	var position 	= location.split("/");
	var pecah		= position[5].split("-");

	if(position[4] == "admin-super")
	{
		option = pecah[1];
	}
	else if(position[4] == "admin-biasa")
	{
		option = pecah[1];
	}
}

function tambah()
{
	option_manage();

	$("#tambah").html("Tunggu Sebentar ...");
	setTimeout(
		function()
		{
			window.location.assign("tambah-"+option);
		},3000
	);	
}

function lihat()
{
	option_manage();

	$("#lihat").html("Tunggu Sebentar ...");
	setTimeout(
		function()
		{
			window.location.assign("lihat-"+option);
		},3000
	);	
}

function kembali()
{
	$("#kembali").html("Tunggu Sebentar ...");
	setTimeout(
		function()
		{
			window.location.assign("beranda")
		},3000
	);	
}

$(document).ready(function()
{
	$("#tambah").click(function()
	{
		tambah();
	});

	$("#lihat").click(function()
	{
		lihat();
	});
		
	$("#kembali").click(function()
	{
		kembali();
	});
});
