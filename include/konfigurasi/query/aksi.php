	<?php
	include 'koneksi.php';
	include 'fungsi.php';

	defineUrl();

	if(isset($_GET['option']))
	{
		switch($_GET['option']) 
		{
			case 'aktif_web':
				try
				{
					$sql		= "UPDATE t_maintenance_web SET status_web='aktif'";
					$queryAdmin	= $koneksi->prepare($sql);
					$queryAdmin->execute();
					
					header("location:$_SERVER[HTTP_REFERER]");
				}
				catch(Exception $e)
				{
					$_SESSION['errorPesanQuery'] = "ERROR QUERY ADMIN KARENA <b>--".$e->getMessage()."--</b>";
					header("location:".BASE_URL."error/query");
				}
			break;
			
			case'nonaktif_web':
				$sql		= "UPDATE t_maintenance_web SET status_web='tidak_aktif'";
					$queryAdmin	= $koneksi->prepare($sql);
					$queryAdmin->execute();
					
					header("location:$_SERVER[HTTP_REFERER]");
			break;

			case 'lock':
				$username = $_SESSION['username'];
				$password = $_POST['password'];
				try
				{
					$sql		= "SELECT * FROM t_admin WHERE admin_username=:username AND admin_password=:password";
					$queryAdmin	= $koneksi->prepare($sql);
					$queryAdmin->bindParam(':username',$username,PDO::PARAM_STR);
					$queryAdmin->bindParam(':password',$password,PDO::PARAM_STR);
					$queryAdmin->execute();
					$rowAdmin = $queryAdmin->rowCount();

					if($rowAdmin == 0)
					{
						$hakAkses = $_SESSION['hakAkses'];
						$hakAkses = str_replace("_", "-", $hakAkses);
						header("location:".BASE_URL."".$hakAkses."/lock");	
					} 
					else
					{
						$hakAkses = $_SESSION['hakAkses'];
						$hakAkses = str_replace("_", "-", $hakAkses);
						header("location:".BASE_URL."".$hakAkses."/beranda");	
					}
				}
				catch(Exception $e)
				{
					$_SESSION['errorPesanQuery'] = "ERROR QUERY ADMIN KARENA <b>--".$e->getMessage()."--</b>";
					header("location:".BASE_URL."error/query");
				}
			break;
			
			case 'login-admin':
				$username		= antiInjection($_POST['username']);
				$password		= antiInjection($_POST['password']);
				$saltPassword	= sha1($password);

				try
				{
					$sql		= "SELECT * FROM t_admin 
									WHERE admin_username=:username 
									AND admin_password=:password 
									AND admin_salt_password=:saltPassword";
					$queryAdmin	= $koneksi->prepare($sql);
					$queryAdmin->bindParam(':username',$username,PDO::PARAM_STR);
					$queryAdmin->bindParam(':password',$password,PDO::PARAM_STR);
					$queryAdmin->bindParam(':saltPassword',$saltPassword,PDO::PARAM_STR);
					$queryAdmin->execute();
					$rowAdmin 	= $queryAdmin->rowCount();
					if($rowAdmin == 0)
					{
						echo "Maaf, Anda Tidak Terdaftar";
						//$_SESSION['pesanLoginAdmin'] = "Maaf, anda tidak terdaftar";
						header("location:$_SERVER[HTTP_REFERER]");
					}
					else
					{
						$dataAdmin = $queryAdmin->fetch();

						$_SESSION['id']				= $dataAdmin['admin_id'];
						$_SESSION['username']		= $dataAdmin['admin_username'];
						$_SESSION['password']		= $dataAdmin['admin_password'];
						$_SESSION['saltPassword']	= $dataAdmin['admin_salt_password'];
						$_SESSION['hakAkses']		= $dataAdmin['admin_hak_akses'];
						$_SESSION['login']			= 1;
						$_SESSION['page'] 			= "";

						try
						{
							$keterangan = "login";
							$sql		= "INSERT INTO t_admin_log (admin_log_admin,admin_log_keterangan)
											VALUES(:adminId,:keterangan)";
							$queryLog	= $koneksi->prepare($sql);
							$queryLog->bindParam(':adminId',$_SESSION['id'],PDO::PARAM_INT);
							$queryLog->bindParam(':keterangan',$keterangan,PDO::PARAM_STR);
							$queryLog->execute();
							$rowLog 	= $queryLog->rowCount();

							echo $_SESSION['hakAkses'];
							if($_SESSION['hakAkses'] == "admin_super"){header("location:".BASE_URL."admin-super/beranda");}
							else if($_SESSION['hakAkses'] == "penyelia"){header("location:".BASE_URL."penyelia/beranda");}
							else if($_SESSION['hakAkses'] == "admin_biasa"){header("location:".BASE_URL."admin-biasa/beranda");}
						}
						catch(Exception $e)
						{
							$_SESSION['errorPesanQuery'] = "ERROR QUERY ADMIN KARENA <b>--".$e->getMessage()."--</b>";
							header("location:".BASE_URL."error/query");			
						}

						//header("location:".BASE_URL."admin/beranda");
					}
				}
				catch(Exception $e)
				{
					$_SESSION['errorPesanQuery'] = "ERROR QUERY ADMIN KARENA <b>--".$e->getMessage()."--</b>";
					header("location:".BASE_URL."error/query");
				}
			break;

			case 'login-admin-super':
				$username		= antiInjection($_POST['username']);
				$password		= antiInjection($_POST['password']);
				$kode			= antiInjection($_POST['kode']);
				$saltPassword	= sha1($password);

				try
				{
					$sql		= "SELECT * FROM t_admin 
									WHERE admin_username=:username 
									AND admin_password=:password 
									AND admin_salt_password=:saltPassword";
					$queryAdmin	= $koneksi->prepare($sql);
					$queryAdmin->bindParam(':username',$username,PDO::PARAM_STR);
					$queryAdmin->bindParam(':password',$password,PDO::PARAM_STR);
					$queryAdmin->bindParam(':saltPassword',$saltPassword,PDO::PARAM_STR);
					$queryAdmin->execute();
					$rowAdmin 	= $queryAdmin->rowCount();
					if($rowAdmin == 0)
					{
						echo "Maaf, Anda Tidak Terdaftar";
						//$_SESSION['pesanLoginAdmin'] = "Maaf, anda tidak terdaftar";
						header("location:$_SERVER[HTTP_REFERER]");
					}
					else
					{
						$sql2		= "SELECT * FROM t_maintenance_web
										WHERE kode_website=:kode";
						$queryAdmin2	= $koneksi->prepare($sql2);
						$queryAdmin2->bindParam(':kode',$kode,PDO::PARAM_STR);
						$queryAdmin2->execute();
						$rowAdmin2 	= $queryAdmin2->rowCount();
						if($rowAdmin2 == 0){echo "kode salah";}
						else{

							$dataAdmin = $queryAdmin->fetch();

							$_SESSION['id']				= $dataAdmin['admin_id'];
							$_SESSION['username']		= $dataAdmin['admin_username'];
							$_SESSION['password']		= $dataAdmin['admin_password'];
							$_SESSION['saltPassword']	= $dataAdmin['admin_salt_password'];
							$_SESSION['hakAkses']		= $dataAdmin['admin_hak_akses'];
							$_SESSION['login']			= 1;
							$_SESSION['page'] 			= "";

							try
							{
								$keterangan = "login";
								$sql		= "INSERT INTO t_admin_log (admin_log_admin,admin_log_keterangan)
												VALUES(:adminId,:keterangan)";
								$queryLog	= $koneksi->prepare($sql);
								$queryLog->bindParam(':adminId',$_SESSION['id'],PDO::PARAM_INT);
								$queryLog->bindParam(':keterangan',$keterangan,PDO::PARAM_STR);
								$queryLog->execute();
								$rowLog 	= $queryLog->rowCount();

								echo $_SESSION['hakAkses'];
								if($_SESSION['hakAkses'] == "admin_super"){header("location:".BASE_URL."admin-super/beranda");}
								else{echo "Maaf, anda tidak memiliki izin";}
							}
							catch(Exception $e)
							{
								$_SESSION['errorPesanQuery'] = "ERROR QUERY ADMIN KARENA <b>--".$e->getMessage()."--</b>";
								header("location:".BASE_URL."error/query");			
							}
						}

						//header("location:".BASE_URL."admin/beranda");
					}
				}
				catch(Exception $e)
				{
					$_SESSION['errorPesanQuery'] = "ERROR QUERY ADMIN KARENA <b>--".$e->getMessage()."--</b>";
					header("location:".BASE_URL."error/query");
				}
			break;

			case 'logout-admin':
				try
				{
					$keterangan = "logout";
					$sql		= "INSERT INTO t_admin_log (admin_log_admin,admin_log_keterangan)
									VALUES(:adminId,:keterangan)";
					$queryLog	= $koneksi->prepare($sql);
					$queryLog->bindParam(':adminId',$_SESSION['id'],PDO::PARAM_INT);
					$queryLog->bindParam(':keterangan',$keterangan,PDO::PARAM_STR);
					$queryLog->execute();
					$rowLog 	= $queryLog->rowCount();
				}
				catch(Exception $e)
				{
					$_SESSION['errorPesanQuery'] = "ERROR QUERY ADMIN KARENA <b>--".$e->getMessage()."--</b>";
					header("location:".BASE_URL."error/query");			
				}

				session_destroy();
				header("location:".BASE_URL."admin/masuk");
			break;

			case 'import-ubf':
				include 'excel_reader2.php';	

				$data = new Spreadsheet_Excel_Reader($_FILES['upload']['tmp_name']);
				$baris = $data->rowcount($sheet_index=0);

				$sukses = 0;
				$gagal = 0;

				for ($i=7; $i<=$baris; $i++)
				{
					$mesin = $data->val($i, 1);
					$wo = $data->val($i, 7);
					$jdwl = $data->val($i, 10);

					$jadwal = date("Y-m-d", strtotime($jdwl));

					

					if($mesin == "")
					{
						exit;
					}
					elseif($mesin == "a")
					{
						continue;
					}

					try 
					{
						$sql		= "SELECT mesin_id FROM t_mesin WHERE mesin_nama=:mesin";
						$queryDepartemen	= $koneksi->prepare($sql);
						$queryDepartemen->bindParam(':mesin',$mesin,PDO::PARAM_STR);
						$queryDepartemen->execute();		

						$a = $queryDepartemen->fetch();

						$b = $a['mesin_id'];
					} 
					catch (Exception $e) 
					{
						
					}

					try 
					{
						$sql		= "SELECT * FROM t_pasangan_mesin WHERE pasangan_mesin_dari=:mesin";
						$queryA	= $koneksi->prepare($sql);
						$queryA->bindParam(':mesin',$b,PDO::PARAM_STR);
						$queryA->execute();		

						$x = $queryA->fetch();

						$y = $x['pasangan_mesin_ke'];
					} 
					catch (Exception $e) 
					{
						echo "err ".$e;
					}

					if($b != 0)
					{
						$sql		= "INSERT INTO t_report_filling	(rf_mesin,rf_no_wo,rf_waktu_jadwal)
										VALUES(:mesin,:wo,:jadwal)";
						$queryDepartemen	= $koneksi->prepare($sql);
						$queryDepartemen->bindParam(':mesin',$b,PDO::PARAM_STR);
						$queryDepartemen->bindParam(':jadwal',$jadwal,PDO::PARAM_STR);
						//$queryDepartemen->bindParam(':packing',$y,PDO::PARAM_STR);
						$queryDepartemen->bindParam(':wo',$wo,PDO::PARAM_STR);
						$queryDepartemen->execute();

						$sql2		= "INSERT INTO t_report_packing	(rp_mesin,rp_no_wo,rp_waktu_jadwal)
										VALUES(:mesin,:wo,:jadwal)";
						$que	= $koneksi->prepare($sql2);
						$que->bindParam(':mesin',$y,PDO::PARAM_STR);
						$que->bindParam(':jadwal',$jadwal,PDO::PARAM_STR);
						$que->bindParam(':wo',$wo,PDO::PARAM_STR);
						$que->execute();
						header("location:$_SERVER[HTTP_REFERER]");
					}
				}
			break;

			case 'import-ubp':
				include 'excel_reader2.php';	

				$data = new Spreadsheet_Excel_Reader($_FILES['upload']['tmp_name']);
				$baris = $data->rowcount($sheet_index=0);

				$sukses = 0;
				$gagal = 0;

				for ($i=8; $i<=$baris; $i++)
				{
					$mesin = $data->val($i, 1);
					$wo1 = $data->val($i, 4);
					$wo2 = $data->val($i, 5);
					$wo = $wo1."".$wo2;
					$jdwl = $data->val($i, 3);

					$jadwal = date("Y-m-d", strtotime($jdwl));

					if($mesin == "")
					{
						exit;
					}

					try 
					{
						$sql		= "SELECT mesin_id FROM t_mesin WHERE mesin_nama=:mesin";
						$queryDepartemen	= $koneksi->prepare($sql);
						$queryDepartemen->bindParam(':mesin',$mesin,PDO::PARAM_STR);
						$queryDepartemen->execute();		

						$a = $queryDepartemen->fetch();

						$b = $a['mesin_id'];
					} 
					catch (Exception $e) 
					{
						
					}

					if($b != 0)
					{
						$sql		= "INSERT INTO t_report_sacktipping	(rs_no_wo,rs_waktu_jadwal)
										VALUES(:wo,:jadwal)";
						$queryDepartemen	= $koneksi->prepare($sql);
						$queryDepartemen->bindParam(':wo',$wo,PDO::PARAM_STR);
						$queryDepartemen->bindParam(':jadwal',$jadwal,PDO::PARAM_STR);
						$queryDepartemen->execute();

						$sql2		= "INSERT INTO t_report_mixing	(rm_mesin,rm_no_wo,rm_waktu_jadwal)
										VALUES(:mesin,:wo,:jadwal)";
						$que	= $koneksi->prepare($sql2);
						$que->bindParam(':mesin',$b,PDO::PARAM_STR);
						$que->bindParam(':wo',$wo,PDO::PARAM_STR);
						$que->bindParam(':jadwal',$jadwal,PDO::PARAM_STR);
						$que->execute();

						$sql3		= "INSERT INTO t_report_discharge	(rd_no_wo,rd_waktu_jadwal)
										VALUES(:wo,:jadwal)";
						$quer	= $koneksi->prepare($sql3);
						$quer->bindParam(':wo',$wo,PDO::PARAM_STR);
						$quer->bindParam(':jadwal',$jadwal,PDO::PARAM_STR);
						$quer->execute();

						header("location:$_SERVER[HTTP_REFERER]");
					}
				}
			break;

			// case 'import-item':
			// 	include 'excel_reader2.php';	

			// 	$data = new Spreadsheet_Excel_Reader($_FILES['upload']['tmp_name']);
			// 	$baris = $data->rowcount($sheet_index=0);

			// 	$sukses = 0;
			// 	$gagal = 0;

			// 	for ($i=144; $i<=$baris; $i++)
			// 	{
			// 		$no_item = $data->val($i, 3);
			// 		$nama_item = $data->val($i, 2);
			// 		$brand_item = $data->val(143, 2);

			// 		$exp = explode(" ", $brand_item);

			// 		$brand = $exp[1];

			// 		if($nama_item == "")
			// 		{
			// 			exit;
			// 		}

			// 		try 
			// 		{
			// 			$sql		= "SELECT brand_id FROM t_brand WHERE brand_nama=:brand";
			// 			$queryDepartemen	= $koneksi->prepare($sql);
			// 			$queryDepartemen->bindParam(':brand',$brand,PDO::PARAM_STR);
			// 			$queryDepartemen->execute();		

			// 			$a = $queryDepartemen->fetch();

			// 			$b = $a['brand_id'];
			// 		} 
			// 		catch (Exception $e) 
			// 		{
						
			// 		}

			// 		if($b != 0)
			// 		{
			// 			$sql		= "INSERT INTO t_item	(item_no,item_brand,item_nama)
			// 							VALUES(:no_item,:brand,:nama_item)";
			// 			$queryDepartemen	= $koneksi->prepare($sql);
			// 			$queryDepartemen->bindParam(':no_item',$no_item,PDO::PARAM_STR);
			// 			$queryDepartemen->bindParam(':brand',$b,PDO::PARAM_STR);
			// 			$queryDepartemen->bindParam(':nama_item',$nama_item,PDO::PARAM_STR);
			// 			$queryDepartemen->execute();
			// 		}
			// 	}
			// break;

			case 'import-mtol':
				include 'excel_reader2.php';	

				$data = new Spreadsheet_Excel_Reader($_FILES['upload']['tmp_name']);
				$baris = $data->rowcount($sheet_index=0);


				$sukses = 0;
				$gagal = 0;

				for ($i=7; $i<=$baris; $i++)
				{
					$prod = $data->val($i, 3);
					$exp = $data->val($i, 37);
					$prod_date = date("Y-m-d", strtotime($prod));
					$exp_date = date("Y-m-d", strtotime($exp)); 

					$no_item = $data->val($i, 7);

					$wo = $data->val($i, 4);
					$wo2 = $data->val($i, 5);
					$no_wo = $wo."".$wo2;	

					$batch1 = $data->val($i, 18);
					$batch2 = $data->val($i, 19);
					$batch3 = $data->val($i, 20);
					$batch4 = $data->val($i, 21);
					$no_batch = $batch1."".$batch2."".$batch3."".$batch4;					

					if($no_batch == "")
					{
						exit;
					}

					try 
					{
						$sql		= "SELECT item_no FROM t_item WHERE item_no=:no_item";
						$queryDepartemen	= $koneksi->prepare($sql);
						$queryDepartemen->bindParam(':no_item',$no_item,PDO::PARAM_STR);
						$queryDepartemen->execute();		

						$a = $queryDepartemen->fetch();

						$b = $a['item_no'];
					} 
					catch (Exception $e) 
					{
						
					}

					if($b != 0)
					{
						$sql		= "INSERT INTO t_wo	(wo_no,wo_item,wo_no_batch,wo_tgl_prod,wo_tgl_exp)
										VALUES(:no_wo,:no_item,:no_batch,:prod_date,:exp_date)";
						$queryDepartemen	= $koneksi->prepare($sql);
						$queryDepartemen->bindParam(':no_wo',$no_wo,PDO::PARAM_STR);
						$queryDepartemen->bindParam(':no_item',$no_item,PDO::PARAM_STR);
						$queryDepartemen->bindParam(':no_batch',$no_batch,PDO::PARAM_STR);
						$queryDepartemen->bindParam(':prod_date',$prod_date,PDO::PARAM_STR);
						$queryDepartemen->bindParam(':exp_date',$exp_date,PDO::PARAM_STR);
						$queryDepartemen->execute();

						$sql2		= "INSERT INTO t_report_gudang_baku	(rgb_no_wo,rgb_waktu_jadwal)
										VALUES(:no_wo,:prod_date)";
						$quer	= $koneksi->prepare($sql2);
						$quer->bindParam(':no_wo',$no_wo,PDO::PARAM_STR);
						$quer->bindParam(':prod_date',$prod_date,PDO::PARAM_STR);
						$quer->execute();

						header("location:$_SERVER[HTTP_REFERER]");
					}
				}
			break;
		}
	}
?>
