<?php
	function defineUrl()
	{
		define('rootCSS', 		'konfigurasi/css/');
		define('rootJS', 		'http://'.$_SERVER['HTTP_HOST'].'/web_monitoring_prd/include/konfigurasi/js/');
		define('rootGambar', 	'http://'.$_SERVER['HTTP_HOST'].'/web_monitoring_prd/include/images/');
		define('rootPage', 		'halaman/');
		define('BASE_URL',		'http://'.$_SERVER['HTTP_HOST'].'/web_monitoring_prd/');
	}

	function setTimeZone()
	{
		$timezone = date_default_timezone_set('Asia/Jakarta');
		$timezone = date_default_timezone_get();
	}

	function antiInjection($sumber)
	{
		$hasil = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($sumber))));
		return($hasil);
	}

	function sessionLogin()
	{
		if(!isset($_SESSION['login']))
		{
			header("location:".BASE_URL."query/logout-admin");
		}
	}

	function sessionNoLogin()
	{
		if(isset($_SESSION['login']))
		{
			header("location:".$_SESSION['page']);		
		}
	}

	function getBulan($bulan)
	{
		switch ($bulan) 
		{
			case '01'	: echo"Januari";		break;
			case '02'	: echo"Februari";		break;
			case '03'	: echo"Maret";			break;
			case '04'	: echo"April";			break;
			case '05'	: echo"Mei";			break;
			case '06'	: echo"Juni";			break;
			case '07'	: echo"Juli";			break;
			case '08'	: echo"Agustus";		break;
			case '09'	: echo"September";		break;
			case '10'	: echo"Oktober";		break;
			case '11'	: echo"November";		break;
			case '12'	: echo"Desember";		break;
			default 	: echo"Tidak Tersedia";	break;
		}
	}

	function terakhirMasuk($source)
	{
		$now = date("D, d-M-Y H:i:s");

		$selisih_detik = (strtotime($now) - strtotime($source));
		$selisih_menit = $selisih_detik / 60;
		$selisih_jam = $selisih_menit / 60;
		$selisih_hari = $selisih_jam / 24;
		$selisih_bulan = $selisih_hari / 30;
		$selisih_tahun = $selisih_bulan / 12;

		if($selisih_menit < 1){echo "baru saja";}
		else if($selisih_jam < 1){echo round($selisih_menit)." menit yang lalu";}
		else if($selisih_hari < 1){echo round($selisih_jam)." jam yang lalu";}
		else if($selisih_bulan < 1){echo round($selisih_hari)." hari yang lalu";}
		else if($selisih_tahun < 1){echo round($selisih_bulan)." bulan yang lalu";}
	}
?>