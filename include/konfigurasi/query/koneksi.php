<?php
	session_start();
	session_name("MONITORING PRD");
	ob_start();

	try
	{
		$koneksi = new PDO($driver.":host=$host;dbname=$dbName", $username, $password, 
							array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
						);
	}
	catch(PDOException $e)
	{
		$_SESSION['errorPesanQuery'] = "GAGAL KONEKSI KARENA <b>--".$e->getMessage()."--</b>";
		header("location:error/query");
	}
?>