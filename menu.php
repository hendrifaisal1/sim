<html>
	<head>
		<title>WEB MONITORING PRD</title>
		<base href='/web_monitoring_prd/'></base>
		<?php
			define('root', 		$_SERVER['DOCUMENT_ROOT'].'/web_monitoring_prd/include/');
			define('rootQuery', 'konfigurasi/query/');

			include root."".rootQuery."koneksi.php";
			include root."".rootQuery."fungsi.php";
			
			defineUrl();	
		?>
		
	</head>
	<?php
		$halaman = $_GET['halaman'];

		if(isset($halaman))
		{
			switch ($halaman)
			{
				case 'beranda'	:	include root."".rootPage."beranda.php";			break;		
				case 'admin'	:	include root."".rootPage."petugas/admin.php";	break;
				default 		:	include root."".rootPage."error/404.html";		break;
			}
		}
	?>
</html>